#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "../includes/ffcgi.h"

#define TOTAL_PARAMS_NUM 21

char *params_table[TOTAL_PARAMS_NUM];
char *params_names[TOTAL_PARAMS_NUM] = {"proxy-nokeepalive","PATH","SERVER_SIGNATURE","SERVER_SOFTWARE","SERVER_NAME",\
                                        "SERVER_ADDR","SERVER_PORT","REMOTE_ADDR","DOCUMENT_ROOT","REQUEST_SCHEME",\
                                        "CONTEXT_PREFIX","CONTEXT_DOCUMENT_ROOT","SERVER_ADMIN","SCRIPT_FILENAME","REMOTE_PORT",\
                                        "GATEWAY_INTERFACE","SERVER_PROTOCOL","REQUEST_METHOD","QUERY_STRING","REQUEST_URI",\
                                        "SCRIPT_NAME"};

/*
0 = proxy-nokeepalive
1 = PATH
2 = SERVER_SIGNATURE

3 = SERVER_SOFTWARE
4 = SERVER_NAME
5 = SERVER_ADDR
6 = SERVER_PORT
7 = REMOTE_ADDR
8 = DOCUMENT_ROOT
9 = REQUEST_SCHEME
10 = CONTEXT_PREFIX
11 = CONTEXT_DOCUMENT_ROOT
12 = SERVER_ADMIN
13 = SCRIPT_FILENAME
14 = REMOTE_PORT
15 = GATEWAY_INTERFACE
16 = SERVER_PROTOCOL
17 = REQUEST_METHOD
18 = QUERY_STRING
19 = REQUEST_URI
20 = SCRIPT_NAME
*/



char** get_table_params(void)
{ return params_table; }

int get_size_params(void) {
  unsigned short size = 0;
  for (int i = 0; i < TOTAL_PARAMS_NUM; i++) {
    size += 2*sizeof(char) + strlen(params_names[i]);
    if ((params_table[i] != NULL) && (params_table[i][0] != 0)) size += strlen(params_table[i]);
  }
  return size;
}

char* get_params(void) {
  short size_param = 0;
  int size = get_size_params();
  //char write_size[2];
  char* params = (char *)malloc(size), *index;
  params[0] = 0;
  index = params;

  for (int i = 0; i < TOTAL_PARAMS_NUM; i++) {
    size_param = strlen(params_names[i]);
    //size_param = htons(size_param);
    //sprintf(write_size, "%x", size_param);
    printf("%x + %s\n",size_param, params_names[i]);
    strcpy(index, (char*)&size_param);
    index = (index + 1);

    if (params_table[i]) size_param = strlen(params_table[i]);
    else size_param = 0;
    //size_param = htons(size_param);
    //sprintf(write_size, "%x", size_param);
    if (params_table[i]) printf("%x + %s\n",size_param, params_table[i]);

    strcpy(index, (char*)&size_param);
    index = (index + 1);

    strcpy(index, params_names[i]);
    index = index + strlen(params_names[i]);
    if (params_table[i] != NULL) {
        strcpy(index, params_table[i]);
        index = index + strlen(params_table[i]);
    }
  }
  for (int i = 0; i < size; i++){
    if (params[i]) printf("%c",params[i]);
    else printf(" ");
  }

  return params;
}


void fill_params_table(void) {
  // fullpath , foldername , filename
  Field** h = get_head();
  char* fullpath = h[TARGET]->content;
  char* foldername = get_folder(fullpath);
  char* filename = get_filename(fullpath);
  char* addr = "127.0.0.1";
  char* proxfcgi = "proxy:fcgi://";
  char* port = "9000";
  char* ver = h[HTTP_VERSION]->content;
  char* meth = h[METHOD]->content;

  char* server_addr = malloc(sizeof(char)*strlen(addr));
  strcpy(server_addr,addr);

  char* server_port = malloc(sizeof(char)*strlen(port));
  strcpy(server_port,port);

  char* remote_addr = malloc(sizeof(char)*strlen(addr));
  strcpy(remote_addr,addr);

  char* document_root = malloc(sizeof(char)*strlen(foldername));
  strcpy(document_root,foldername);

  char* context_document_root = malloc(sizeof(char)*strlen(foldername));
  strcpy(context_document_root,foldername);

  char* script_filename = malloc(sizeof(char)*(strlen(addr)+strlen(proxfcgi)+2+strlen(port)+strlen(fullpath)));
  strcpy(script_filename,proxfcgi);
  strcat(script_filename,addr);
  strcat(script_filename,":");
  strcat(script_filename,port);
  strcat(script_filename,"/");
  strcat(script_filename,fullpath);


  char* server_prot = malloc(sizeof(char)*strlen(ver));
  strcpy(server_prot,ver);

  char* request_uri = malloc(sizeof(char)*(1+strlen(filename)));
  strcpy(request_uri,"/");
  strcat(request_uri,filename);

  char* script_name = malloc(sizeof(char)*(1+strlen(filename)));
  strcpy(script_name,"/");
  strcat(script_name,filename);

  char* request_method = malloc(sizeof(char)*strlen(meth));
  strcpy(request_method,meth);


  /* ---- FILL PARAMETERS ----- */

  params_table[0] = (char *)malloc(sizeof(char)*1);
  strcpy(params_table[0], "0\0");

  params_table[1] = (char *)malloc(sizeof(char)*59);
  strcpy(params_table[1],"/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin/bin\0");

  //SERVER_SIGNATURE
  // params_table[2];

  //SERVER_SOFTWARE
  // params_table[3];

  //SERVER_NAME
  // params_table[4];

  //SERVER_ADDR
  params_table[5] = server_addr;

  //SERVER_PORT
  params_table[6] = (char *)malloc(sizeof(char)*2);
  strcpy(params_table[6], "80\0");

  //REMOTE_ADDR
  params_table[7] = remote_addr;

  //DOCUMENT_ROOT
  params_table[8] = foldername;

  //REQUEST_SCHEME
  params_table[9] = (char *)malloc(sizeof(char)*4);
  strcpy(params_table[9], "http\0");

  //CONTEXT_PREFIX
  // params_table[10];

  //CONTEXT_DOCUMENT_ROOT
  params_table[11] = context_document_root;

  //SERVER_ADMIN
  // params_table[12];

  //SCRIPT_FILENAME
  params_table[13] = script_filename;

  //REMOTE_PORT
  // params_table[14];

  //GATEWAY_INTERFACE
  // params_table[15];

  //SERVER_PROTOCOL
  // params_table[16];

  //REQUEST_METHOD
  params_table[17] = request_method;

  //QUERY_STRING
  // params_table[18];

  //REQUEST_URI
  params_table[19] = request_uri;
  strcat(params_table[19],"\0");

  //SCRIPT_NAME
  params_table[20] = script_name;
  strcat(params_table[20],"\0");

}

// =========================================================================================================== //
void writeSocket(int fd,FCGI_Header *h,unsigned int len)
{
	int w;

	h->contentLength=htons(h->contentLength);
	h->paddingLength=htons(h->paddingLength);


	while (len) {
	// try to write
		do {
			w = write(fd, h, len);
		} while (w == -1 && errno == EINTR);
	len-=w;
	}
}

// =========================================================================================================== //
void writeLen(int len, char **p) {
	if (len > 0x7F ) {
		*((*p)++)=(len>>24)&0x7F;
		*((*p)++)=(len>>16)&0xFF;
		*((*p)++)=(len>>8)&0xFF;
		*((*p)++)=(len)&0xFF;
	} else *((*p)++)=(len)&0x7F;
}

// =========================================================================================================== //
// int addNameValuePair(FCGI_Header *h,char *name,char *value)
// {
// 	char *p;
// 	unsigned int nameLen=0,valueLen=0;
//
// 	if (name) nameLen=strlen(name);
// 	if (value) valueLen=strlen(value);
//
// 	if ((valueLen > FASTCGIMAXNVPAIR) || (valueLen > FASTCGIMAXNVPAIR) ) return -1;
// 	if ((h->contentLength+((nameLen>0x7F)?4:1)+((valueLen>0x7F)?4:1)) > FASTCGILENGTH ) return -1;
//
// 	p=(h->contentData)+h->contentLength;
// 	writeLen(nameLen,&p);
// 	writeLen(valueLen,&p);
// 	strncpy(p,name,nameLen);
// 	p+=nameLen;
// 	if (value) strncpy(p,value,valueLen);
// 	h->contentLength+=nameLen+((nameLen>0x7F)?4:1);
// 	h->contentLength+=valueLen+((valueLen>0x7F)?4:1);
// }
// =========================================================================================================== //
// void sendGetValue(int fd)
// {
// FCGI_Header h;
//
// 	h.version=FCGI_VERSION_1;
// 	h.type=FCGI_GET_VALUES;
// 	h.requestId=htons(FCGI_NULL_REQUEST_ID);
// 	h.contentLength=0;
// 	h.paddingLength=0;
// 	addNameValuePair(&h,FCGI_MAX_CONNS,NULL);
// 	addNameValuePair(&h,FCGI_MAX_REQS,NULL);
// 	addNameValuePair(&h,FCGI_MPXS_CONNS,NULL);
// 	writeSocket(fd,&h,FCGI_HEADER_SIZE+(h.contentLength)+(h.paddingLength));
// }
// =========================================================================================================== //
void sendBeginRequest(int fd,unsigned short requestId,unsigned short role,unsigned char flags)
{
FCGI_Header h;
FCGI_BeginRequestBody *begin;

	h.version=FCGI_VERSION_1;
	h.type=FCGI_BEGIN_REQUEST;
	h.requestId=htons(requestId);
	h.contentLength=sizeof(FCGI_BeginRequestBody);
	h.paddingLength=0;
	begin=(FCGI_BeginRequestBody *)&(h.contentData);
	begin->role=htons(role);
	begin->flags=flags;
	writeSocket(fd,&h,FCGI_HEADER_SIZE+(h.contentLength)+(h.paddingLength));
}
// =========================================================================================================== //
void sendAbortRequest(int fd,unsigned short requestId)
{
FCGI_Header h;

	h.version=FCGI_VERSION_1;
	h.type=htons(FCGI_ABORT_REQUEST);
	h.requestId=requestId;
	h.contentLength=0;
	h.paddingLength=0;
	writeSocket(fd,&h,FCGI_HEADER_SIZE+(h.contentLength)+(h.paddingLength));
}
#define sendStdin(fd,id,stdin,len) sendWebData(fd,FCGI_STDIN,id,stdin,len)
#define sendData(fd,id,data,len) sendWebData(fd,FCGI_DATA,id,data,len)
//============================================================================================================ //
void sendWebData(int fd,unsigned char type,unsigned short requestId,char *data,unsigned int len)
{
FCGI_Header h;

	if (len > FASTCGILENGTH) return ;

	h.version=FCGI_VERSION_1;
	h.type=type;
	h.requestId=htons(requestId);
	h.contentLength=len;
	h.paddingLength=0;
	memcpy(h.contentData,data,len);
	writeSocket(fd,&h,FCGI_HEADER_SIZE+(h.contentLength)+(h.paddingLength));
}

// =========================================================================================================== //
static int createSocket(int port)
{
	int fd;
	struct sockaddr_in serv_addr;
	int enable = 1;

	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket creation failed\n");
		return (-1);
	}

	bzero(&serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	inet_aton("127.0.0.1",&serv_addr.sin_addr);
	serv_addr.sin_port = htons(port);

	if (connect(fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		perror("connect failed\n");
		return (-1);
	}

	return fd;
}

char* readSocket(int fd,int len)
{
				if (len == -1)
				{
					FCGI_Header h;
					read(fd,&h,FCGI_HEADER_SIZE);
					len = h.contentLength;
				}
				char* data = malloc((1+len)*sizeof(char));
				read(fd,data,len);
				data[len] = 0;
				return data;
}
// =========================================================================================================== //

#define MAX_STDOUTS_COUNT 100
char* php(int* body_len)
{
				printf("checkpoint B.0\n");
				unsigned char type_received;
				//--initialisations mise en tampon---------------------
				char* buffer = NULL;
	            char** buffers = (char**)calloc(MAX_STDOUTS_COUNT,sizeof(char*)); //tableau contenant les buffers successifs
				int *lengthBuffers = calloc(MAX_STDOUTS_COUNT,sizeof(int)); //tableau contenant la longueur des buffers successifs
	            int b = 0; //pour écrire dans les deux tableaux précédents
	            int totalLength = 0; //longueur totale des buffers contenus dans le tableau
				//-------
				char* rbuffer = NULL;
				char* trash;
				FCGI_Header* lu = malloc(sizeof(FCGI_Header));

				// On génère le bloc de donnée de params
				fill_params_table();
				printf("checkpoint B.1\n");
				char* params = get_params();
				printf("checkpoint B.2\n");
				char* pparams = params;
				printf("checkpoint B.3\n");
				int size = get_size_params();
				int tmp;
				//

				int fd = createSocket(9000);
				sendBeginRequest(fd,1,FCGI_RESPONDER,0);

				do {
					tmp = (size < FASTCGILENGTH) ? size : FASTCGILENGTH;
					sendWebData(fd,FCGI_PARAMS,1,params,tmp);
					size -= tmp;
					params += tmp;
				} while(size);
				free(pparams);

				sendStdin(fd,1,NULL,0);

				do {
					read(fd,lu,FCGI_HEADER_SIZE);
					type_received = lu->type;
					rbuffer = readSocket(fd,lu->contentLength);
					if (lu->paddingLength)
					{
						read(fd,trash,lu->paddingLength);
						free(trash);
					}
					if (type_received == FCGI_STDOUT)
					{
						//mise en tampon
						lengthBuffers[b] = lu-> contentLength;;
                        totalLength += lu-> contentLength;;
                        buffers[b++] = rbuffer;
					}
					free(lu);
				} while(type_received != FCGI_END_REQUEST);

				close(fd);

				buffer = (char*)malloc((totalLength + 1) * sizeof(char));
				*body_len = totalLength - lengthBuffers[0];
				tmp = 0;
				for (int i=0;i<b;i++)
				{
					memcpy(buffer+tmp,buffers[i],lengthBuffers[i]);
					tmp += lengthBuffers[i];
					free(buffers[i]);
		        }
			    free(lengthBuffers);
			    free(buffers);
			    buffer[tmp] = 0;
			    return buffer;
}
