#include "request_line.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include "fastcgi.h"


//params.c
char** get_table_params(void);
char* get_params(void);
void fill_params_table(void);
char* php(int* body_len);
