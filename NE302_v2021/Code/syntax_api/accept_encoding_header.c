#include "../includes/syntax.h"

/* ----------------------------------ACCEPT_ENCODING_HEADER---------------------------------- */


//content-coding = token
const_p content_coding (const_p s)
{ return token(s); }

//codings : content-coding / "identity" / "*"
const_p codings (const_p s) {
  const_p cursor = content_coding(s);
  if (cursor == NULL) cursor = ruleor("identity", NULL, "*", NULL, s);
  return cursor;
}

//_codings_maybe_weight = ( codings [ weight ] )
const_p _codings_maybe_weight (const_p s)
{ return rulefac(NULL, codings, NULL, weight, s); }

//ows_and_codings_maybe_weight = OWS ( codings [ weight ] )
const_p ows_and_codings_maybe_weight (const_p s)
{ return rulecat(NULL, ows, NULL, _codings_maybe_weight, s); }

//single_part2_accept_encoding = OWS "," [ OWS ( codings [ weight ] ) ]
const_p single_part2_accept_encoding (const_p s)
{ return rulefac(NULL, ows_and_comma, NULL, ows_and_codings_maybe_weight, s); }

//_part1_accept_encoding = "," / ( codings [ weight ] )
const_p _part1_accept_encoding (const_p s)
{ return ruleor(",", NULL, NULL, _codings_maybe_weight, s); }

//_part2_accept_encoding = * ( OWS "," [ OWS ( codings [ weight ] ) ] )
const_p _part2_accept_encoding (const_p s)
{ return rulestar(NULL, single_part2_accept_encoding, 0,INF,s); }

//Accept-Encoding : [ ( "," / ( codings [ weight ] ) ) * ( OWS "," [ OWS ( codings [ weight ] ) ] ) ]
const_p accept_encoding (const_p s) {
  const_p cursor = rulecat(NULL, _part1_accept_encoding, NULL, _part2_accept_encoding, s);
  if (cursor == NULL) cursor = s;
  return cursor;
}

//Accept-Encoding-header : "Accept-Encoding" ":" OWS Accept-Encoding OWS
const_p accept_encoding_header (const_p s) {
  const_p cursor = rulecat("Accept-Encoding:", NULL, NULL, ows, s);
  if (cursor != NULL) {
    cursor = rulecat(NULL, accept_encoding, NULL, ows, cursor);
  }
  return cursor;
}
