#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef TYPE
#define TYPE
	typedef const char* const_p;
	typedef const_p (*Rule)(const_p);
#endif

#define check(p) if(!p){printf("error\n");exit(-1);}
#define INF -1

/*s     => La ou on test*/
const_p ruleint(const_p bot,const_p top,const_p s);
const_p rulechar(const_p test,const_p s);
const_p ruleword(const_p test,const_p s);
const_p rulecase(const_p test,const_p s);
/*testX => Si on test un char*/
/*ruleX => Si on test une regle*/
const_p rulecat(const_p test1,\
                Rule rule1,\
                const_p test2,\
                Rule rule2,\
                const_p s);
/*min => Nb d'occurence min*/
/*max => Nb d'occurence max*/
const_p rulestar(const_p test,\
                Rule rule,\
                unsigned int min,\
                unsigned int max,\
                const_p s);
const_p ruleor(const_p test1,\
                Rule rule1,\
                const_p test2,\
                Rule rule2,\
                const_p s);
//La 2eme regle est facultative
const_p rulefac(const_p test1,\
                Rule rule1,\
                const_p test2,\
                Rule rule2,\
                const_p s);
//La 1er regle est facultative
const_p rulecaf(const_p test1,\
                Rule rule1,\
                const_p test2,\
                Rule rule2,\
                const_p s);

const_p cr(const_p s);
const_p lf(const_p s);
const_p crlf(const_p s);
const_p sp(const_p s);
const_p htab(const_p s);
const_p wsp(const_p s);
const_p dquote(const_p s);
const_p digit(const_p s);
const_p alpha(const_p s);
const_p hex(const_p s);
const_p vchar(const_p s);
const_p octet(const_p s);
const_p obs_text(const_p s);
const_p ctl(const_p s);
const_p ows(const_p s);
const_p rws(const_p s);

//request_line.c
const_p sub_delim(const_p s);
const_p pct_encoded(const_p s);
const_p unreserved(const_p s);

const_p pchar(const_p s);
const_p segment(const_p s);
const_p tchar(const_p s);

const_p http_name(const_p s);
const_p query(const_p s);
const_p absolute_path(const_p s);
const_p origin_form(const_p s);
const_p token(const_p s);

const_p http_version(const_p s);
const_p request_target(const_p s);
const_p method(const_p s);

const_p request_line(const_p s);
//request_line.c

//status_line.c
const_p reason_phrase(const_p s);
const_p status_code(const_p s);
const_p status_line(const_p s);
//status_line.c

//message.c
const_p nomDuHeader(const_p s);
const_p implemented_only(const_p s);

const_p _other(const_p s);
const_p message_body(const_p s);
const_p obs_fold(const_p s);
const_p field_name(const_p s);
const_p field_vchar(const_p s);
const_p field_content(const_p s);
const_p field_value(const_p s);
const_p start_line(const_p s);
const_p _header_fields(const_p s);
const_p http_message(const_p s);
//message.c

//host_header.c
const_p h16(const_p s);
const_p ls32(const_p s);
const_p dec_octet(const_p s);
const_p ipvfuture(const_p s);
const_p ipv6address(const_p s);
const_p reg_name(const_p s);
const_p ipv4address(const_p s);
const_p ip_literal(const_p s);
const_p host(const_p s);
const_p port(const_p s);
const_p uri_host(const_p s);
const_p Host(const_p s);
const_p host_header(const_p s);
//host_header.c

//connection_header.c
const_p connection_option(const_p s);
const_p connection_header(const_p s);
const_p connection_string(const_p s);
const_p connection(const_p);
//connection_header.c

//refer_header.c
  const_p referer_header (const_p s);
  const_p referer (const_p s);
  const_p segment_nz (const_p s);
  const_p segment_nz_nc(const_p s);
  const_p path_abempty (const_p s);
  const_p path_noscheme (const_p s);
  const_p path_empty(const_p s);
  const_p path_rootless (const_p s);
  const_p path_absolute (const_p s);
  const_p userinfo (const_p s);
  const_p authority (const_p s);
  const_p hier_part (const_p s);
  const_p relative_part (const_p s);
  const_p scheme (const_p s);
	const_p question_mark_query(const_p s);
  const_p absolute_uri(const_p s);
  const_p partial_uri (const_p s);
//refer_header.c

//accept_language_header.c
  const_p comma_and_ows (const_p s);
  const_p ows_and_comma (const_p s);
  const_p endless_comma_and_ows (const_p s);
  const_p qvalue (const_p s);
  const_p weight (const_p s);
  const_p alphanum (const_p s);
  const_p language_range (const_p s);
	const_p ows_and_language_range_maybe_weight (const_p s);
	const_p single_part2_accept_language (const_p s);
  const_p accept_language (const_p s);
  const_p accept_language_header (const_p s);
//accept_language_header.c

//accept_encoding_header.c
  const_p content_coding (const_p s);
  const_p codings (const_p s);
	const_p single_part2_accept_encoding (const_p s);
	const_p ows_and_codings_maybe_weight (const_p s);
  const_p accept_encoding (const_p s);
  const_p accept_encoding_header (const_p s);
//accept_encoding_header.c

//accept_charset_header.c
  const_p charset (const_p s);
	const_p charset_or_asterisk (const_p s);
	const_p ows_and_charset_maybe_weight (const_p s);
	const_p single_part2_accept_charset (const_p s);
  const_p accept_charset (const_p s);
  const_p accept_charset_header (const_p s);
//accept_charset_header.c

//accept_header.c
  const_p accept_ext (const_p s);
  const_p accept_params (const_p s);
  const_p media_range (const_p s);
	const_p ows_and_media_range_maybe_accept_params (const_p s);
	const_p single_part2_accept (const_p s);
  const_p accept_ (const_p s);
  const_p accept_header (const_p s);
//accept_header.c


//content_length_header.c
const_p content_length(const_p s);
const_p content_length_header(const_p s);
//content_length_header.c

//content_type_header.c
const_p quoted_pair(const_p s);
const_p qdtext(const_p s);
const_p quoted_string(const_p s);
const_p parameter(const_p s);
const_p subtype(const_p s);
const_p type(const_p s);
const_p media_type(const_p s);
const_p content_type(const_p s);
const_p content_type_header(const_p s);
//content_type_header.c

//cookie_header.c
const_p cookie_pair_split(const_p s);

const_p cookie_octet(const_p s);
const_p cookie_value(const_p s);
const_p cookie_name(const_p s);
const_p cookie_pair(const_p s);
const_p cookie_string(const_p s);
const_p cookie_header(const_p s);
//cookie_header.c

//transfer_encoding_header.c
const_p transfer_coding_split(const_p s);
const_p _3transfer_encoding(const_p s);

const_p bws(const_p s);
const_p transfer_parameter(const_p s);
const_p transfer_extension(const_p s);
const_p transfer_coding(const_p s);
const_p transfer_encoding(const_p s);
const_p transfer_encoding_header(const_p s);
//transfer_encoding_header.c

//expect_header.c
const_p expect(const_p s);
const_p expect_header(const_p s);
//expect_header.c

//user_agent_header.c
const_p ctext(const_p s);
const_p product_version(const_p s);
const_p pro_com(const_p s);
const_p comment(const_p s);
const_p product(const_p s);
const_p user_agent(const_p s);
const_p user_agent_header(const_p s);
//user_agent_header.c
#ifndef ALL
#define ALL 0
#endif
#define CC(p) printf("%p",p)
#define QQ printf(".|");
