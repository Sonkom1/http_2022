#include "../includes/syntax.h"

/* Loïs RAJAONSON

    Transfer-Encoding-header =    "Transfer-Encoding" ":" OWS Transfer-Encoding OWS
            Transfer-Encoding =     * ( "," OWS ) transfer-coding * ( OWS "," [ OWS transfer-coding ] )
                transfer-coding =    "chunked" / "compress" / "deflate" / "gzip" / transfer-extension
v                    transfer-extension =    token * ( OWS ";" OWS transfer-parameter )
v                        transfer-parameter =    token BWS "=" BWS ( token / quoted-string )
v                            BWS =    OWS
*/
//transfer coding split: chaque [ows transfer-coding] est séparé par un ows ,
const_p transfer_coding_split(const_p s)
{
    const_p cursor = rulecat(NULL,ows,",",NULL,s);
    if (cursor) cursor =ows(cursor);
    return cursor;
}
// BWS =    OWS
const_p bws(const_p s)
{
    //printf("\n\tbws :%s",s);
    const_p cursor = ows(s);
    return cursor;
}


//_transfer_parameter = ( token / quoted-string )
const_p _transfer_parameter(const_p s)
{
    const_p cursor = ruleor(NULL,token,NULL,quoted_string,s);
    return cursor;
}

//transfer-parameter =    token BWS "=" BWS ( token / quoted-string )
const_p transfer_parameter(const_p s)
{
    //printf("\n\ttransfer_parameter :%s",s);
    const_p cursor = rulecat(NULL,token,NULL,bws,s);
    if (cursor !=NULL)
      {
        cursor = rulecat("=",NULL,NULL,bws,cursor);
        if (cursor !=NULL)
          {
            cursor = _transfer_parameter(cursor);
          }
        
      }
    return cursor;
}

//_transfer-extension =    ( OWS ";" OWS transfer-parameter )
const_p _transfer_extension(const_p s)
{
    const_p cursor = rulecat(NULL,ows,";",NULL,s);
    if (cursor !=NULL)
      {
        cursor = rulecat(NULL,ows,NULL,transfer_parameter,cursor);        
      }
    return cursor;
}

//transfer-extension =    token * ( OWS ";" OWS transfer-parameter )
const_p transfer_extension(const_p s)
{
    //printf("\n\ttransfer_extension :%s",s);
    const_p cursor = token(s);
    if (cursor !=NULL)
      {
        cursor = rulestar(NULL,_transfer_extension,0,INF,cursor);      
      }
    return cursor;
}

//transfer-coding =    "chunked" / "compress" / "deflate" / "gzip" / transfer-extension
const_p transfer_coding(const_p s)
{
    //printf("\n\ttransfer_coding :%s",s);
    const_p cursor = s;
    cursor = rulecase("chunked",s);
    if (cursor == NULL)
      {
        cursor = rulecase("compress",s);
        if (cursor == NULL)
          {
            cursor = rulecase("gzip",s);
            if (cursor == NULL)
              {
                cursor = transfer_extension(s);
              }
          }
      }
    return cursor;
}

//_1transfer-encoding = "," OWS
const_p _1transfer_encoding(const_p s)
{    
    return rulecat(",",NULL,NULL,ows,s);}

//_transfer-encodingn = * ("," OWS)
const_p _star_1transfer_encoding(const_p s)
{   
    return rulestar(NULL,_1transfer_encoding,0,INF,s);}

//_2transfer-encoding = OWS ","
const_p _2transfer_encoding(const_p s)
{return rulecat(NULL,ows,",",NULL,s);}

//_3transfer_encoding = OWS transfer-coding
const_p _3transfer_encoding(const_p s)
{return rulecat(NULL,ows,NULL,transfer_coding,s);}

//_4transfer_encoding = OWS "," [ OWS connection-option ] == rulefac _2c _3c
const_p _4transfer_encoding(const_p s)
{    return rulefac(NULL,_2transfer_encoding,NULL,_3transfer_encoding,s);}

//_star_4transfer_encodingn = * (_4connection)
const_p _star_4transfer_encoding(const_p s)
{    //printf("\n\t_star_4connection :%s",s);
    return rulestar(NULL,_4transfer_encoding,0,INF,s);}

//Transfer-Encoding =     * ( "," OWS ) transfer-coding * ( OWS "," [ OWS transfer-coding ] )
const_p transfer_encoding(const_p s)
{
    const_p cursor = rulecat(NULL,_star_1transfer_encoding, NULL, transfer_coding,s);
    if (cursor != NULL)
        {
        cursor = _star_4transfer_encoding(cursor);
        }
    return cursor;  
}

//Transfer-Encoding-header =    "Transfer-Encoding" ":" OWS Transfer-Encoding OWS
//cookie-header =    "Cookie:" OWS cookie-string OWS
const_p transfer_encoding_header(const_p s)
{
    //printf("\n\tcookie-header :%s",s);
    const_p cursor = rulecase("transfer-encoding", s);
    if (cursor != NULL)
    {
        cursor = rulecat(":", NULL, NULL, ows, cursor);
        if (cursor != NULL)
        {
            cursor = rulecat(NULL, transfer_encoding, NULL, ows, cursor);
        }
    }
    return cursor;
}

























