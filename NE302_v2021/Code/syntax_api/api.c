#include "../includes/api.h"

_Token *api_list = NULL;

void *getRootTree()
{ return (void*)r00t;}


void fill_api_chained_list(node* to_fill){
  _Token *fill, *buffer;
  if(api_list == NULL) {
    api_list = (_Token *)malloc(sizeof(_Token));
    api_list->node = to_fill;
    api_list->next = NULL;
  } else {
    buffer = api_list;
    while (buffer != NULL) {
      fill = buffer;
      buffer = buffer->next;
    }
    fill->next = (_Token *)malloc(sizeof(_Token));
    fill = fill->next;
    fill->node = to_fill;
    fill->next = NULL;
  }
}

_Token *searchTree(void *start,char *name) {
  node* search = start;
  if (search == NULL) search = getRootTree();

  if (strcmp(search->tag,name) == 0)
    fill_api_chained_list(search);

  if (search->left != NULL) searchTree(search->left, name);
  if (search->right != NULL) searchTree(search->right, name);
  if (search->mid != NULL) searchTree(search->mid, name);
  if (search->next != NULL) searchTree(search->next, name);

  return api_list;
}

//Print API_Chained_List
void print_api_list(void) {
  _Token* print_api = api_list;
  node* printed_node;
  while(print_api != NULL) {
    printed_node = print_api->node;
    printf("Field : %s\n",printed_node->tag);
    printf("Value : ");
    for (int i = 0; i < printed_node->len; i++) printf("%c",*((printed_node->value) + i));
    printf("\n\n");
    print_api = print_api->next;
  }
}

char *getElementTag(void *node,int *len)
{ return (char*)((leaf*)node)->tag; }

char *getElementValue(void *node,int *len)
{
  *len = ((leaf*)node)->len;
  return (char*)((leaf*)node)->value;
}

void purgeAPI(void)
{
  _Token *to_purge = NULL, *buffer = api_list;
  api_list = NULL;
  while (buffer != NULL) {
    to_purge = buffer;
    buffer = buffer->next;
    free(to_purge);
  }
}

void purgeElement(_Token **r)
{
    if (r != NULL){
        if (*r!=NULL){
            if ((*r)->next == NULL){
                free(*r);
            }else{
                purgeElement(&((*r)->next));
                free(*r);
            }
        }
    }
}

void purgeTree(void *root) { clear_node(root);  }

int parseur(char *req, int len)
{  r00t = http_message_node(req); return (r00t) ? 1 : 0; }


//
