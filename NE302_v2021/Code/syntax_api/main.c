#include "../includes/tree.h"

void test_rulecat(void)
{
  const_p s = "' isOk";
  printf("Test rulecat : ");
  s = rulecat("'",NULL,NULL,wsp,s);
  check(s);
  printf("%4s\n",s);
}

void test_rulestar(void)
{
  const_p s = "\r\r\risOk";
  printf("Test rulestar : ");
  s = rulestar(NULL,cr,0,3,s);
  check(s);
  printf("%4s\n",s);
}

void test_rulefac(void)
{
  const_p s = "\r\nisOk";
  printf("Test rulefac : ");
  s = rulefac(NULL,cr,NULL,lf,s);
  check(s);
  printf("%4s\n",s);
}

void test_ruleor(void)
{
  const_p s ="\nisOk";
  printf("Test ruleor : ");
  s = ruleor(NULL,cr,NULL,lf,s);
  check(s);
  printf("%4s\n",s);
}

void test_ruleword(void)
{
  const_p s = "LamaisOk";
  printf("Test ruleword : ");
  s = ruleword("Lama",s);
  check(s);
  printf("%4s\n",s);
}

void test_request_line(void)
{
  const_p s = "GET /index.html HTTP/1.1\r\nisOk";
  printf("Test request_line : ");
  s = request_line(s);
  check(s);
  printf("%s\n",s);
}

void test_status_line(void)
{
  const_p s = "HTTP/1.1 200 OK\r\nisOk";
  printf("Test status_line : ");
  s = status_line(s);
  check(s);
  printf("%s\n",s);
}

void test_start_line(void)
{
  const_p a = "HTTP/1.1 200 OK\r\n";
  const_p r = "GET /index.html HTTP/1.1\r\n";
  printf("Test start-line : ");
  r = start_line(r);
  a = start_line(a);
  check(a);
  printf(":dab: la reponse\n");
  check(r);
  printf(":dab:dab: le requete\n");
}

void test_host_header(void)
{
  const_p s = "Host: m.media-amazon.com\r\nisOk";
  printf("Test Host : ");
  s = host_header(s);
  check(s);
  printf("%s\n",s);
}

void test_ipv6(void)
{
  const_p s = "::";
  printf("Test IPv6 : ");
  s = ipv6address(s);
  check(s);
  printf("%s\n",s);
}

void _(void)
{
	node* root;
	const_p s = "hOSt: www.cvedetails.com\r\n";
  const_p ss = "User-Agent: Wget/1.16 (linux-gnu)\r\n";
  const_p sss = "GET / HTTP/1.1\r\n";
  const_p ssss = "HTTP/1.1 200 OKBOOMER\r\n";
  const_p sssss = "Expect: 100-continue\r\n";

	root = host_node(s);
	show_tree(root);
	clear_node(root);
  printf("\n");

  root = user_agent_node(ss);
  show_tree(root);
  clear_node(root);
  printf("\n");

  root = request_line_node(sss);
	show_tree(root);
	clear_node(root);
	printf("\n");

  root = status_line_node(ssss);
	show_tree(root);
	clear_node(root);
	printf("\n");

  root = expect_node(sssss);
	show_tree(root);
	clear_node(root);
	printf("\n");
}

void ___(void)
{
    //const_p s = "Host: www.sendnudes.sale\r\nHost: sendnudes.sale\r\nHost: www.s.sale\r\n";
    const_p s = "HTTP/1.1 200 OK\r\nAccept-Charset: utf-8, iso-8859-1;q=0.5\r\n\r\n";
    const_p ss = "HTTP/1.1 200 OK\r\nUser-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0 (Windows NT 10.0; x64; rv:87.0)\r\n\r\n";
    r00t = http_message_node(s);
    show_tree(r00t);
    clear_node(r00t);
}

int main(int argc,char* argv[])
{
  check_nodes();
  return 0;
}
