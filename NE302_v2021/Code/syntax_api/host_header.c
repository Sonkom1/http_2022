#include "../includes/syntax.h"
/*
Host-header = "Host" ":" OWS Host OWS
  Host = uri-host [ ":" port ]
    uri-host = host
    port = * DIGIT
      host = IP-literal / IPv4address / reg-name
        IP-literal = "[" ( IPv6address / IPvFuture ) "]"
        IPv4address = dec-octet "." dec-octet "." dec-octet "." dec-octet
        reg-name = * ( unreserved / pct-encoded / sub-delims )
          IPv6address = 6 ( h16 ":" ) ls32 / "::" 5 ( h16 ":" ) ls32 / [ h16 ] "::" 4 ( h16 ":" ) ls32 / [ h16 *1 ( ":" h16 ) ] "::" 3 ( h16 ":" ) ls32 / [ h16 *2 ( ":" h16 ) ] "::" 2 ( h16 ":" ) ls32 / [ h16 *3 ( ":" h16 ) ] "::" h16 ":" ls32 / [ h16 *4 ( ":" h16 ) ] "::" ls32 / [ h16 *5 ( ":" h16 ) ] "::" h16 / [ h16 *6 ( ":" h16 ) ] "::"
          IPvFuture = "v" 1* HEXDIG "." 1* ( unreserved / sub-delims / ":" )
          dec-octet = "25" %x30-35 / "2" %x30-34 DIGIT / "1" 2 DIGIT / %x31-39 DIGIT / DIGIT
            ls32 = ( h16 ":" h16 ) / IPv4address
            h16 = 1*4 HEXDIG
*/
 //h16 = 1*4 HEXDIG
const_p h16(const_p s)
 {   return rulestar(NULL,hex,1,4,s); }

//_ls32 = h16 ":" h16
const_p _ls32(const_p s)
{
  const_p cursor = h16(s);
  if (cursor != NULL) cursor = rulecat(":",NULL,NULL,h16,cursor);
  return cursor;
}

//ls32 = _ls32 / IPv4address
const_p ls32(const_p s)
 {  return ruleor(NULL,_ls32,NULL,ipv4address,s); }

//dec-octet = "25" %x30-35 / "2" %x30-34 DIGIT / "1" 2 DIGIT / %x31-39 DIGIT / DIGIT
const_p dec_octet(const_p s)
{
  // "25" %x30-35
  const_p cursor = ruleword("25",s);
  if (cursor != NULL) cursor = ruleint("0","6",cursor);

  // "2" %x30-34 DIGIT
  if (cursor == NULL)
  {
    cursor = rulechar("2",s);
    if (cursor != NULL) cursor = ruleint("0","5",cursor);
    if (cursor != NULL) cursor = digit(cursor);
  }

  // "1" 2 DIGIT
  if (cursor == NULL)
  {
    cursor = rulechar("1",s);
    if (cursor != NULL) cursor = rulestar(NULL,digit,2,2,cursor);
  }

  // %x31-39 DIGIT
  if (cursor == NULL)
  {
    cursor = ruleint("1","9",s);
    if (cursor != NULL) cursor = digit(cursor);
  }

  // DIGIT
  if (cursor == NULL)  cursor = digit(s);

  return cursor;
}

//_ipvfuture : unreserved / sub-delims / ":"
const_p _ipvfuture(const_p s)
{
  const_p cursor = rulechar(":",s);
  if (cursor == NULL) cursor = ruleor(NULL,unreserved,NULL,sub_delim,s);
  return cursor;
}

//IPvFuture = "v" 1* HEXDIG "." 1* ( _ipvfuture )
const_p ipvfuture(const_p s)
{
  const_p cursor = rulechar("v",s);
    if (cursor != NULL)   cursor = rulestar(NULL,hex,1,INF,cursor);
    if (cursor != NULL)   cursor = rulechar(".",cursor);
    if (cursor != NULL)   cursor = rulestar(NULL,_ipvfuture,1,INF,cursor);
  return cursor;
}

const_p _ipv6address(const_p s)
{  return rulecat(":",NULL,NULL,h16,s); }

const_p _piv6address(const_p s)
{  return rulecat(NULL,h16,":",NULL,s); }

const_p _1ipv6address(const_p s)
{
  const_p cursor = h16(s);
  return rulestar(NULL,_ipv6address,0,1,cursor);
}

const_p _2ipv6address(const_p s)
{
  const_p cursor = h16(s);
  return rulestar(NULL,_ipv6address,0,2,cursor);
}
const_p _3ipv6address(const_p s)
{
  const_p cursor = h16(s);
  return rulestar(NULL,_ipv6address,0,3,cursor);
}
const_p _4ipv6address(const_p s)
{
  const_p cursor = h16(s);
  return rulestar(NULL,_ipv6address,0,4,cursor);
}
const_p _5ipv6address(const_p s)
{
  const_p cursor = h16(s);
  return rulestar(NULL,_ipv6address,0,5,cursor);
}

const_p _6ipv6address(const_p s)
{
  const_p cursor = h16(s);
  return rulestar(NULL,_ipv6address,0,6,cursor);
}
/*
IPv6address = 6 ( h16 ":" ) ls32 /
                "::" 5 ( h16 ":" ) ls32 /
                [ h16 ] "::" 4 ( h16 ":" ) ls32 /
                [ h16 *1 ( ":" h16 ) ] "::" 3 ( h16 ":" ) ls32 /
                [ h16 *2 ( ":" h16 ) ] "::" 2 ( h16 ":" ) ls32 /
                [ h16 *3 ( ":" h16 ) ] "::" h16 ":" ls32 /
                [ h16 *4 ( ":" h16 ) ] "::" ls32 /
                [ h16 *5 ( ":" h16 ) ] "::" h16 /
                [ h16 *6 ( ":" h16 ) ] "::"
*/
const_p ipv6address(const_p s)
{
  // 6 ( h16 ":" ) ls32
  const_p cursor = rulestar(NULL,_piv6address,6,6,s);
  if (cursor != NULL) cursor = ls32(cursor);

  //"::" 5 ( h16 ":" ) ls32
  if (cursor == NULL) cursor = ruleword("::",s);
  {
    if (cursor != NULL) cursor = rulestar(NULL,_piv6address,5,5,cursor);
    if (cursor != NULL) cursor = ls32(cursor);
  }

  //[ h16 ] "::" 4 ( h16 ":" ) ls32
  if (cursor == NULL) cursor = rulecaf(NULL,h16,"::",NULL,s);
  {
    if (cursor != NULL) cursor = rulestar(NULL,_piv6address,4,4,cursor);
    if (cursor != NULL) cursor = ls32(cursor);
  }

  //[ h16 *6 ( ":" h16 ) ] "::"
  if (cursor == NULL ) cursor = rulecaf(NULL,_6ipv6address,"::",NULL,s);

  //[ h16 *5 ( ":" h16 ) ] "::" h16
  if (cursor == NULL ) cursor = rulecaf(NULL,_5ipv6address,"::",NULL,s);
  {
    if (cursor != NULL) cursor = h16(cursor);
  }

  //[ h16 *4 ( ":" h16 ) ] "::" ls32
  if (cursor == NULL ) cursor = rulecaf(NULL,_4ipv6address,"::",NULL,s);
  {
    if (cursor != NULL) cursor = ls32(cursor);
  }

  // [ h16 *3 ( ":" h16 ) ] "::" h16 ":" ls32
  if (cursor == NULL ) cursor = rulecaf(NULL,_3ipv6address,"::",NULL,s);
  {
    if (cursor != NULL) cursor = rulecat(NULL,h16,":",NULL,cursor);
    if (cursor != NULL) cursor = ls32(cursor);
  }

  // [ h16 *2 ( ":" h16 ) ] "::" 2 ( h16 ":" ) ls32
  if (cursor == NULL ) cursor = rulecaf(NULL,_2ipv6address,"::",NULL,s);
  {
    if (cursor != NULL) cursor = rulestar(NULL,_piv6address,2,2,cursor);
    if (cursor != NULL) cursor = ls32(cursor);
  }

  // [ h16 *1 ( ":" h16 ) ] "::" 3 ( h16 ":" ) ls32
  if (cursor == NULL ) cursor = rulecaf(NULL,_1ipv6address,"::",NULL,s);
  {
    if (cursor != NULL) cursor = rulestar(NULL,_piv6address,3,3,cursor);
    if (cursor != NULL) cursor = ls32(cursor);
  }

  return cursor;
}

//_reg-name = unreserved / pct-encoded / sub-delims
const_p _reg_name(const_p s)
{
  const_p cursor = unreserved(s);
  if (cursor == NULL) cursor = ruleor(NULL,pct_encoded,NULL,sub_delim,s);
  return cursor;
}

//reg-name = *(_reg_name)
const_p reg_name(const_p s)
{  return rulestar(NULL,_reg_name,0,INF,s); }

//_ivp4address = "." dec-octet
const_p _ipv4address(const_p s)
{ return rulecat(".",NULL,NULL,dec_octet,s); }

//IPv4address = dec-octet 3 (_ivp4address)
const_p ipv4address(const_p s)
{
  const_p cursor = dec_octet(s);
  if (cursor != NULL) cursor = rulestar(NULL,_ipv4address,3,3,cursor);
  return cursor;
}

//IP-literal = "[" ( IPv6address / IPvFuture ) "]"
const_p ip_literal(const_p s)
{
  const_p cursor = rulechar("[",s);
  if (cursor != NULL) cursor = ruleor(NULL,ipv6address,NULL,ipvfuture,cursor);
  if (cursor != NULL) cursor = rulechar("]",cursor);
  return cursor;
}

//host = IP-literal / IPv4address / reg-name
const_p host(const_p s)
{
  const_p cursor = ip_literal(s);
  if (cursor == NULL) cursor = ruleor(NULL,ipv4address,NULL,reg_name,s);
  return cursor;
}

//port = * DIGIT
const_p port(const_p s)
{ return rulestar(NULL,digit,0,INF,s); }

//uri-host = host
const_p uri_host(const_p s)
{ return host(s); }


//_Host = ":" port
const_p _Host(const_p s)
{ return rulecat(":",NULL,NULL,port,s); }

//Host = uri-host [_Host]
const_p Host(const_p s)
{ return rulefac(NULL,uri_host,NULL,_Host,s); }

//Host-header = "Host" ":" OWS Host OWS
const_p host_header(const_p s)
{
  const_p cursor = rulecase("Host:",s);
  if (cursor != NULL) cursor = ows(cursor);
  if (cursor != NULL) cursor = rulecat(NULL,Host,NULL,ows,cursor);
  return cursor;
}
