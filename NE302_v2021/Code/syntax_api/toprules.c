#include "../includes/api.h"
/*
node* regle_node(const_p s)
{
    node* to_return;
    Rule r[4] = {&regle_header,&regle};//rules
    Rule e[4] = {&ows,&ows};//exception
    to_return = toprules(r,e,s);
    if (to_return != NULL && to_return->len > 0 && to_return->left)
      {
       //Traitement des sous branches
      }
    return to_return;
}
*/
///           FINI            ///
node* connection_node(const_p s)
{
  node* to_return;
  Rule r[4] = {&connection_header,&connection};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  if (to_return != NULL && to_return->len > 0 && to_return->left)
  {
    s = to_return->left->value;
    to_return->left->left = connection_option_node(s);
  }
  return to_return;
}

node* connection_option_node(const_p s)
{
    node* to_return;
    to_return = new_node(&connection_option,"connection-option",s);
    s = connection_option(s);
    while (transfer_coding_split(s)) //ce n'est pas une erreur, le séparateur est le même donc flemme de refaire 2 fois la même fonction.
    {
        s = transfer_coding_split(s);
    }
    s = ows(s);
    if (s) {
        to_return->next = connection_option_node(s);
        }    
    else {}
    return to_return;
}
///           FINI            ///

node* content_length_node(const_p s)
{
  node* to_return;
  Rule r[4] = {&content_length_header,&content_length};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  return to_return;
}

node* content_type_node(const_p s)
{
  node* to_return;
  Rule r[4] = {&content_type_header,&content_type};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  if (to_return != NULL && to_return->len > 0 && to_return->left)
  {
    to_return->left->tag = "content-type";
    s = to_return->left->value;
    to_return->left->left = new_node(&media_type,"media-type",s);
  }
  return to_return;
 }

node* cookie_node(const_p s)
{
  node* to_return;
  Rule r[4] = {&cookie_header,&cookie_string};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  if (to_return != NULL && to_return->len > 0 && to_return->left)
  {
    s = to_return->left->value;
    to_return->left->left = cookie_pair_node(s);
  }
  return to_return;
}

node* cookie_pair_node(const_p s)
{
    node* to_return = new_leaf_sp(&cookie_pair,NULL,&cookie_pair_split,"cookie_pair",NULL,s);
    if (to_return)
    {
        to_return->left = new_node(&cookie_name,"cookie_name",s);
        s = cookie_name(s);
        s = ruleword("=",s);
        to_return->mid = new_node(&cookie_value,"cookie_value",s);
        s = cookie_value(s);
        s = cookie_pair_split(s);
        node* parcours = to_return->next;

    while(parcours != NULL)
      {
        parcours->left = new_node(&cookie_name,"cookie_name",s);
        s = cookie_name(s);
        s = ruleword("=",s);
        parcours->mid = new_node(&cookie_value,"cookie_value",s);
        s = cookie_value(s);
        s = cookie_pair_split(s);
        parcours = parcours->next;
      }
    }
    return to_return;
}

node* transfer_encoding_node(const_p s)
{
  node* to_return;
  Rule r[4] = {&transfer_encoding_header,&transfer_encoding};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  if (to_return != NULL && to_return->len > 0 && to_return->left)
  {
    s = to_return->left->value;
    //to_return->left->left = new_leaf(&transfer_coding,NULL,"transfer_coding",NULL,s);
    to_return->left->left = transfer_coding_node(s);
  }
  return to_return;
}

node* transfer_coding_node(const_p s)
{
    node* to_return;
    to_return = new_node(&transfer_coding,"transfer-coding",s);
    if (transfer_extension(s) && !rulecase("chunked",s) && !rulecase("compress",s) && !rulecase("gzip",s))
    {
    to_return->left = new_node(&transfer_extension,"transfer_extension",s);
    }
    s = transfer_coding(s);
    while (transfer_coding_split(s))
    {
        s = transfer_coding_split(s);
    }
    s = ows(s);
    if (s) {
        to_return->next = transfer_coding_node(s);
        }
    else {printf("/!\ comportement inattendu");}
    return to_return;
}

///           FINI            ///
node* host_node(const_p s)
{
    node* to_return;
    Rule r[4] = {&host_header,&Host};//rules
    Rule e[4] = {&ows,&ows};//exception
    to_return = toprules(r,e,s);
    if (to_return != NULL && to_return->len > 0 && to_return->left)
      {
        s = to_return->left->value;
        to_return->left->left = new_node(reg_name,"reg_name",s);
        if (to_return->left->left == NULL) to_return->left->left = new_node(ipv4address,"IPv4address",s);
        if (to_return->left->left == NULL) to_return->left->left = new_node(ip_literal,NULL,s);
      }
    return to_return;
  }
///           FINI            ///

///           FINI            ///
node* accept_node(const_p s)
{
  node* to_return, *node_cursor;
  Rule r[4] = {&accept_header,&accept_};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  if (to_return != NULL && to_return->len > 0 && to_return->left)
  {
    s = to_return->left->value;
    to_return->left->left = new_node(&media_range,"media_range",s);
    node_cursor = to_return->left->left;
    if (node_cursor != NULL) {
      s = (node_cursor->value + node_cursor->len);
      if (accept_params(s) != NULL) {
        node_cursor->next = new_node(&accept_params,"Accept_params",s);
        node_cursor = node_cursor->next;
        s = (node_cursor->value + node_cursor->len);
      }
      while (single_part2_accept(s) != NULL) {
        s = rulecat(NULL, ows, ",", NULL, s);
        if (ows_and_media_range_maybe_accept_params(s) != NULL) {
          s = ows(s);
          node_cursor->next = new_node(&media_range,"media_range",s);
          node_cursor = node_cursor->next;
          s = (node_cursor->value + node_cursor->len);
          if (accept_params(s) != NULL) {
            node_cursor->next = new_node(&accept_params,"Accept_params",s);
            node_cursor = node_cursor->next;
            s = (node_cursor->value + node_cursor->len);
          }
        }
      }
    }
  }
  return to_return;
}
///           FINI            ///

node* accept_charset_node(const_p s)
{
  node* to_return, *node_cursor;
  Rule r[4] = {&accept_charset_header,&accept_charset};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  if (to_return != NULL && to_return->len > 0 && to_return->left)
  {
    s = to_return->left->value;
    s = endless_comma_and_ows(s);
    if (charset(s) != NULL) {
      to_return->left->left = new_node(&charset,"charset",s);
      node_cursor = to_return->left->left;
    }
    s = charset_or_asterisk(s);
    if (weight(s) != NULL) {
      if (node_cursor == NULL) {
        to_return->left->left = new_node(&weight,"weight",s);
        node_cursor = to_return->left->left;
      } else {
        node_cursor->next = new_node(&weight,"weight",s);
        node_cursor = node_cursor->next;
      }
      s = (node_cursor->value + node_cursor->len);
    }
    while (single_part2_accept_charset(s) != NULL) {
      s = rulecat(NULL, ows, ",", NULL, s);
      if (ows_and_charset_maybe_weight(s) != NULL) {
        s = ows(s);
        if (charset(s) != NULL) {
          if (node_cursor == NULL) {
            to_return->left->left = new_node(&charset,"charset",s);
            node_cursor = to_return->left->left;
          } else {
            node_cursor->next = new_node(&charset,"charset",s);
            node_cursor = node_cursor->next;
          }
        }
        s = charset_or_asterisk(s);
        if (weight(s) != NULL) {
          if (node_cursor == NULL) {
            to_return->left->left = new_node(&weight,"weight",s);
            node_cursor = to_return->left->left;
          } else {
            node_cursor->next = new_node(&weight,"weight",s);
            node_cursor = node_cursor->next;
          }
          s = (node_cursor->value + node_cursor->len);
        }
      }
    }
  }
  return to_return;
}

node* accept_encoding_node(const_p s)
{
  node *to_return, *node_cursor;
  Rule r[4] = {&accept_encoding_header,&accept_encoding};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  if (to_return != NULL && to_return->len > 0 && to_return->left)
  {
    s = to_return->left->value;
    to_return->left->left = new_node(&codings,"codings",s);
    node_cursor = to_return->left->left;
    if (node_cursor != NULL) {
      s = (node_cursor->value + node_cursor->len);
      if (weight(s) != NULL) {
        node_cursor->next = new_node(&weight,"weight",s);
        node_cursor = node_cursor->next;
        s = (node_cursor->value + node_cursor->len);
      }
      while (single_part2_accept_encoding(s) != NULL) {
        s = rulecat(NULL, ows, ",", NULL, s);
        if (ows_and_codings_maybe_weight(s) != NULL) {
          s = ows(s);
          node_cursor->next = new_node(&codings,"codings",s);
          node_cursor = node_cursor->next;
          s = (node_cursor->value + node_cursor->len);
          if (weight(s) != NULL) {
            node_cursor->next = new_node(&weight,"weight",s);
            node_cursor = node_cursor->next;
            s = (node_cursor->value + node_cursor->len);
          }
        }
      }
    }
  }
  return to_return;
}

node* accept_language_node(const_p s)
{
  node *to_return, *node_cursor;
  Rule r[4] = {&accept_language_header,&accept_language};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  if (to_return != NULL && to_return->len > 0 && to_return->left)
  {
    s = endless_comma_and_ows(to_return->left->value);
    to_return->left->left = new_node(&language_range,"language_range",s);
    node_cursor = to_return->left->left;
    s = (node_cursor->value + node_cursor->len);

    if (weight(s) != NULL) {
      node_cursor->next = new_node(&weight,"weight",s);
      node_cursor = node_cursor->next;
      s = (node_cursor->value + node_cursor->len);
    }

    while (single_part2_accept_language(s) != NULL) {
      s = rulecat(NULL, ows, ",", NULL, s);
      if (ows_and_language_range_maybe_weight(s) != NULL) {
        s = ows(s);
        node_cursor->next = new_node(&language_range,"language_range",s);
        node_cursor = node_cursor->next;
        s = (node_cursor->value + node_cursor->len);

        if (weight(s) != NULL) {
          node_cursor->next = new_node(&weight,"weight",s);
          node_cursor = node_cursor->next;
          s = (node_cursor->value + node_cursor->len);
        }
      }
    }
  }
  return to_return;
}

node* referer_node(const_p s)
{
    node* to_return, *node_cursor;
    Rule r[4] = {&referer_header,&referer};//rules
    Rule e[4] = {&ows,&ows};//exception
    to_return = toprules(r,e,s);
    if (to_return != NULL && to_return->len > 0 && to_return->left)
      {
        s = to_return->left->value;
        if (partial_uri(s) != NULL) {
          to_return->left->left = new_node(&partial_uri,"partial-URI",s);
          node_cursor = to_return->left->left;
          node_cursor->left = new_node(&relative_part,"relative_part",s);
          s = (node_cursor->left->value + node_cursor->left->len);
          if (question_mark_query(s) != NULL) {
            s = rulecase("?",s);
            node_cursor->mid = new_node(&query,"query",s);
          }
          node_cursor = node_cursor->left;

          s = rulecase("//", node_cursor->value);
          node_cursor->left = new_node(&authority,"authority",s);
          node_cursor = node_cursor->left;

          s = node_cursor->value;
          if(rulecat(NULL, userinfo, "@", NULL, s) != NULL) {
            node_cursor->left = new_node(&userinfo,"userinfo",s);
            s = rulecase("@", node_cursor->left->value);
          }

          node_cursor->mid = new_node(&host, "host", s);
          s = node_cursor->mid->value + node_cursor->mid->len;

          if(rulecat(":", NULL, NULL, port, s) != NULL) node_cursor->right = new_node(&port,"port",s);

        } else {
          to_return->left->left = new_node(&absolute_uri,"absolute_URI",s);
          node_cursor = to_return->left->left;

          node_cursor->left = new_node(&scheme,"scheme",s);
          s = rulecat(NULL, scheme, ":", NULL, s);

          node_cursor->mid = new_node(&hier_part,"hier_part",s);
          s = (node_cursor->mid->value + node_cursor->mid->len);
          if (question_mark_query(s) != NULL) {
            s = rulecase("?",s);
            node_cursor->right = new_node(&query,"query",s);
          }
          node_cursor = node_cursor->mid;

          s = rulecase("//", node_cursor->value);
          node_cursor->left = new_node(&authority,"authority",s);
          node_cursor = node_cursor->left;

          s = node_cursor->value;
          if(rulecat(NULL, userinfo, "@", NULL, s) != NULL) {
            node_cursor->left = new_node(&userinfo,"userinfo",s);
            s = rulecat(NULL, userinfo, "@", NULL, s);
          }

          node_cursor->mid = new_node(&host, "host", s);
          s = node_cursor->mid->value + node_cursor->mid->len;

          if(rulecat(":", NULL, NULL, port, s) != NULL) node_cursor->right = new_node(&port,"port",s);
        }

        // to_return->left->left = new_node(absolute_uri,"absolute-URI",s);
        // if (to_return->left->left == NULL) to_return->left->left = new_node(partial_uri,"partial-URI",s);
      }
    return to_return;
  }

///           FINI            ///
node* user_agent_node(const_p s)
{
    node* to_return;
    Rule r[4] = {&user_agent_header,&user_agent};//rules
    Rule e[4] = {&ows,&ows};//exception
    to_return = toprules(r,e,s);
    if (to_return != NULL && to_return->len > 0 && to_return->left)
      {
        s = to_return->left->value;
        to_return->left->left = new_node(&product,"product",s);
        s += to_return->left->left->len;
        leaf* cursor_l = to_return->left;
        leaf* cursor_r = NULL;
        while (s = rws(s))
          {
            if (product(s))
              {
                cursor_l->next = new_node(&product,"product",s);
                cursor_l = cursor_l->next;
                s += cursor_l->len;
              }
            else if(comment(s))
              {
                if (cursor_r == NULL)
                  {
                    to_return->mid = new_node(&comment,"comment",s);
                    cursor_r = to_return->mid;
                  }
                else
                  {
                    cursor_r->next = new_node(&comment,"comment",s);
                    cursor_r = cursor_r->next;
                  }
                s += cursor_r->len;
              }
          }
      }
    return to_return;
  }
///           FINI            ///
///           FINI            ///
node* expect_node(const_p s)
{
  Rule r[4] = {&expect_header,&expect};//rules
  Rule e[4] = {&ows,&ows};//exception
  return toprules(r,e,s);
}
///           FINI            ///

///           FINI            ///
node* request_line_node(const_p s)
{
  Rule r[4] = {&request_line,&method,&request_target,&http_version};
  Rule e[4] = {NULL,&sp,&sp,&crlf};
  return toprules(r,e,s);
}
///           FINI            ///
///           FINI            ///
node* status_line_node(const_p s)
{
  Rule r[4] = {&status_line,&http_version,&status_code,&reason_phrase};
  Rule e[4] = {NULL,&sp,&sp,&crlf};
  return toprules(r,e,s);
}
///           FINI            ///
///           FINI            ///

node* http_message_node(const_p s)
{
  node* to_return;
  node* cursor;
  Rule r[4] = {&http_message,&start_line};
  Rule e[4] = {NULL,NULL};
  to_return = toprules(r,e,s);
  if (to_return)
    {
      //Pour start-line
      to_return->left->left = (request_line(s)) ? request_line_node(s) : status_line_node(s);
      s = to_return->left->value + to_return->left->len;
      //Pour les header-fields
      to_return->mid = new_leaf(&_header_fields,NULL,NULL,NULL,s);
      if (to_return->mid)
      {
        if(implemented_only(s))
            {
            name_it(to_return->mid);      
            to_return->mid = noder(to_return->mid); 
            cursor = to_return->mid;
            s = _header_fields(s);
            } else{
                to_return->mid->tag = nomDuHeader(s);
                to_return->mid->left = new_node(&field_name,"field_name",s);
                
                
                
                to_return->mid->value = s;
                to_return->mid->len = _other(s) - s;
                
                s = ruleword(to_return->mid->tag,s);
                s = ruleword(":",s);
                s  = ows(s);
                to_return->mid->mid = new_node(&field_value,"field_value",s);
                s = field_value(s);
                s = ows(s);
                s = crlf(s);
                cursor = to_return->mid;
            }
            while(cursor && cursor->next)
                {  
                if(implemented_only(s))
                    {
                    if (cursor->next->tag == NULL){name_it(cursor->next);}
                    cursor->next = noder(cursor->next);
                    cursor = cursor->next;
                    s = _header_fields(s);
                    }
                
                else 
                    {
                    
                    
                    
                    
                    cursor->next->tag = nomDuHeader(s); //je nomme le header non implémenté
                    
                    cursor->next->left = new_node(&field_name,"field_name",s);
                    
                    
                    s = ruleword(cursor->next->tag,s);
                    s = ruleword(":",s);
                    s  = ows(s); //j'avance s jusqu'au field-value de ce header non implémenté
                    
                    cursor->next->mid = new_node(&field_value,"field_value",s);
                    cursor->next->value = s; //la valeur du noeud du header non implémenté est mise à s
                    cursor->next->len = field_value(s) - s; //sa longueur est mise à field_value(s) - s

                    s = field_value(s); //on avance s après la field-value
                    s = crlf(s);
                    s = ows(s);;
                    cursor = cursor->next;//le curseur passe au suivant
                    
                    
                    
                    
                    
                    
                    }
                //s = _header_fields(s);
                }
            s = cursor->value + cursor->len;
            crlf(s);
            }   
        }
      //Pour le body
      s = crlf(s);
      if (s)
        {
          to_return->right = new_node(&message_body,"body",s);
        }
  return to_return;
}

//VERSION DU DOCKER
/*
node* http_message_node(const_p s)
{
  node* to_return;
  node* cursor;
  Rule r[4] = {&http_message,&start_line};
  Rule e[4] = {NULL,NULL};
  to_return = toprules(r,e,s);
  if (to_return)
    {
      //Pour start-line
      to_return->left->left = (request_line(s)) ? request_line_node(s) : status_line_node(s);
      s = to_return->left->value + to_return->left->len;
      //Pour les header-fields
      to_return->mid = new_leaf(&_header_fields,NULL,NULL,NULL,s);
      if (to_return->mid)
        {
          name_them(to_return->mid);
          to_return->mid = noder(to_return->mid);
          cursor = to_return->mid;
          while(cursor && cursor->next)
            {
              cursor->next = noder(cursor->next);
              cursor = cursor->next;
            }
          s = cursor->value + cursor->len;
          crlf(s);
        }
      //Pour le body
      s = crlf(s);
      if (s)
        {
          to_return->right = new_node(&message_body,"body",s);
        }
     }
  return to_return;
}*/

///           FINI            ///
void check_nodes(void)
{
  const_p s = "GET /?username=lama HTTP/1.0\r\nReferer: https://mail.esisar.grenoble-inp.fr/?username=nicolas.grosjean1@etu.esisar.grenoble-inp.fr\r\nAccept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Charset: utf-8, iso-8859-1;q=0.5\r\nAccept: image/webp,*/*\r\nConnection: keep-alive\r\nHost: mail.esisar.grenoble-inp.fr\r\nUser-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0\r\nAccept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3\r\n\r\nLama.";
  node* root = http_message_node(s);
  tab_show_tree(root,0);
  clear_node(root);
}
