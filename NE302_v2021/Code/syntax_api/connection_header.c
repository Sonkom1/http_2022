#include "../includes/syntax.h"

/* Loïs RAJAONSON

connection-header = "Connection" ":" OWS Connection OWS

    Connection = * ( "," OWS ) connection-option * ( OWS "," [ OWS connection-option ] )

        connection-option = token

            token = 1* tchar

                tchar = ...

*/


//connection-option = token
const_p connection_option(const_p s)
{
    //printf("\n\tconnection-option :%s",s);
    const_p cursor = token(s);
    return cursor;
}


//_1connection = "," OWS
const_p _1connection(const_p s)
{    //printf("\n\t_1connection :%s",s);
    return rulecat(",",NULL,NULL,ows,s);}

//_star_1connection = * ("," OWS)
const_p _star_1connection(const_p s)
{   //printf("\n\t_star_1connection :%s",s);
    return rulestar(NULL,_1connection,0,INF,s);}

//_2connection = OWS ","
const_p _2connection(const_p s)
{return rulecat(NULL,ows,",",NULL,s);}

//_3connection = OWS connection-option
const_p _3connection(const_p s)
{return rulecat(NULL,ows,NULL,connection_option,s);}

//_4connection = OWS "," [ OWS connection-option ] == rulefac _2c _3c
const_p _4connection(const_p s)
{    return rulefac(NULL,_2connection,NULL,_3connection,s);}

//_star_4connection = * (_4connection)
const_p _star_4connection(const_p s)
{    //printf("\n\t_star_4connection :%s",s);
    return rulestar(NULL,_4connection,0,INF,s);}


//Connection = * ( "," OWS ) connection-option * ( OWS "," [ OWS connection-option ] ) == star_1c star_3c
const_p connection(const_p s)
{
    const_p cursor = rulecat(NULL,_star_1connection, NULL, connection_option,s);
    if (cursor != NULL)
        {
        cursor = _star_4connection(cursor);
        }
    return cursor;
}


//version artisanale de ruleword.
const_p connection_string(const_p s)
{
    /*if (s == NULL)
    {return NULL;}
    int indice_caractere=-1;
    char is_connection[11] = "connection\0";
    while (((s != NULL) && (*s == is_connection[++indice_caractere])) && (is_connection[indice_caractere] != '\0'))
    {++s;}
    if (indice_caractere == 10)
        {return s;}
    else
        {return NULL;}
    return s;*/
    const_p cursor = rulecase("connection",s);
}


//connection-header = "Connection" ":" OWS Connection OWS
const_p connection_header(const_p s)
{
    //printf("\n\tconnection_header :%s,s");
    const_p cursor = rulecat(NULL, connection_string, ":", NULL, s);
    if (cursor != NULL)
    {
        cursor = rulecat(NULL, ows, NULL, connection, cursor);
        if (cursor != NULL)
        {
            cursor = ows(cursor);
        }
    }
    return cursor;
}
