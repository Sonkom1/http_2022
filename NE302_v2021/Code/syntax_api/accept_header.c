#include "../includes/syntax.h"
/*
Accept-header
Accept-Charset-header
Accept-Encoding-header
Accept-Language-header
Referer-header
*/

/* ----------------------------------ACCEPT_HEADER---------------------------------- */


//_equals_and_token_or_quotedstring = "=" ( token / quoted-string )
const_p _equals_and_token_or_quotedstring (const_p s) {
  const_p cursor = ruleword("=", s);
  if (cursor != NULL) cursor = ruleor(NULL, token, NULL, quoted_string, cursor);
  return cursor;
}

//accept-ext = OWS ";" OWS token [ "=" ( token / quoted-string ) ]
const_p accept_ext (const_p s) {
  const_p cursor = rulecat(NULL, ows, ";", NULL, s);
  if (cursor != NULL) cursor = ows(cursor);
  if (cursor != NULL) cursor = rulefac(NULL, token, NULL, _equals_and_token_or_quotedstring, cursor);
  return cursor;
}
//accept-params = weight * accept-ext
const_p accept_params (const_p s) {
  const_p cursor = weight(s);
  if (cursor != NULL) cursor = rulestar(NULL, accept_ext, 0, INF, cursor);
  return cursor;
}

//_type_slash_subtype = ( type "/" subtype )
const_p _type_slash_subtype (const_p s) {
  const_p cursor = type(s);
  if (cursor != NULL) cursor = rulecat("/", NULL, NULL, subtype, cursor);
  return cursor;
}

//_type_slash = ( type "/*" )
const_p _type_slash (const_p s)
{ return rulecat(NULL, type, "/*", NULL, s); }

//_part1_media_range = ( "*/*" / ( type "/" subtype ) / ( type "/*" ) )
const_p _part1_media_range (const_p s) {
  const_p cursor = ruleword("*/*", s);
  if (cursor == NULL) cursor = ruleor(NULL, _type_slash_subtype, NULL, _type_slash, s);
  return cursor;
}
//_single_part2_media_range = ( OWS ";" OWS parameter )
const_p _single_part2_media_range (const_p s) {
  const_p cursor = rulecat(NULL, ows, ";", NULL, s);
  if (cursor != NULL) cursor = rulecat(NULL, ows, NULL, parameter, cursor);
  return cursor;
}

//_part2_media_range =  * ( OWS ";" OWS parameter )
const_p _part2_media_range (const_p s)
{ return rulestar(NULL, _single_part2_media_range, 0, INF, s); }

//media-range = ( "*/*" / ( type "/" subtype ) / ( type "/*" ) ) * ( OWS ";" OWS parameter )
const_p media_range (const_p s)
{ return rulecat(NULL, _part1_media_range, NULL, _part2_media_range, s); }


//_media_range_maybe_accept_params = ( media-range [ accept-params ] )
const_p _media_range_maybe_accept_params (const_p s)
{ return rulefac(NULL, media_range, NULL, accept_params, s); }

//ows_and_media_range_maybe_accept_params = OWS ( media-range [ accept-params ] )
const_p ows_and_media_range_maybe_accept_params (const_p s)
{ return rulecat(NULL, ows, NULL, _media_range_maybe_accept_params, s); }

//single_part2_accept = OWS "," [ OWS ( media-range [ accept-params ] ) ]
const_p single_part2_accept (const_p s)
{ return rulefac(NULL, ows_and_comma, NULL, ows_and_media_range_maybe_accept_params, s); }

//_part1_accept = "," / ( media-range [ accept-params ] )
const_p _part1_accept (const_p s)
{ return ruleor(",", NULL, NULL, _media_range_maybe_accept_params, s); }

//_part2_accept = * ( OWS "," [ OWS ( media-range [ accept-params ] ) ] )
const_p _part2_accept (const_p s)
{ return rulestar(NULL, single_part2_accept, 0,INF,s); }

//Accept : [ ( "," / ( media-range [ accept-params ] ) ) * ( OWS "," [ OWS ( media-range [ accept-params ] ) ] ) ]
const_p accept_ (const_p s) {
  const_p cursor = rulecat(NULL, _part1_accept, NULL, _part2_accept, s);

  if (cursor == NULL) cursor = s;
  return cursor;
}

//Accept-header : "Accept" ":" OWS Accept OWS
const_p accept_header (const_p s) {
  const_p cursor = rulecat("Accept:", NULL, NULL, ows, s);
  if (cursor != NULL) {
    cursor = rulecat(NULL, accept_, NULL, ows, cursor);
  }
  return cursor;
}
