#include "../includes/parse.h"
/*void test(void)
{
	printf("Input de test :\t%s\n",req);

	printf("Résultat des *len et *show :\n\t");
	wshow(req);
	printf("[GET] a %ld[3] lettres\n\t",wlen(req));
	lshow(req);
	printf(" a %ld[3] mots\n",llen(req));

	printf("\n");

	printf("Résultat de split :\n\t");
	lama* pos = malloc(llen(req)*sizeof(lama));
	split(pos,req,0);
	for (char i = 0 ; i < llen(req) ; i++)
		wshow(req+pos[i]);
	printf("\n");

	printf("Résultat de pshow :\n\t");
	pshow(pos,llen(req),req);
	printf("\n");

	printf("Resultat des rector:\n");
	line* lin = dissector(req,0);
	fshow(lin,req);

	printf("\n\nResultat de merge:");
	merge(lin,0);
	fshow(lin,req);

	printf("\n");

	merge(lin,1);
	fshow(lin,req);
	printf("\n");

	merge(lin,0);
	fshow(lin,req);
	printf("\n");

	printf("Resultat de forget :\n");
	line* f = dissector(req,0);
	forget(&f,0);
	fshow(f,f->start);
	printf("\n");

	clearector(f);
	clearector(lin);
	free(pos);
}


int main(int argc,char* argv[])
{
	test();
	return 0;
}*/

/*-------------- Word & Line --------------*/

//Retourne le nombre de lettre dans un "mot"
lama wlen(const char* text)
{
	const char c = *text;
	return (c == ' ' || c == '\r' || c == '\n' || c == 0) ? 0 : 1+wlen(++text);
}

//Retourne le nombre de mots dans une "ligne"
lama llen(const char* text)
{
	const char c = *text;
	return (c == '\r' || c == '\n' || c == 0) ? 0 : 1+llen(text+1+wlen(text));
}

//Affiche le "mot"
void wshow(const char* text)
{
	const char c = *text;
	if (!(c == ' ' ||  c == '\r' || c == '\n' || c == 0))
		{	printf("%c",*text);	wshow(++text);	}
}

//Affiche la "ligne"
void lshow(const char* text)
{
	const char c = *text;
	if (!(c == '\r' || c == '\n' || c == 0 ))
		{	wshow(text); printf(" ");	lshow(text+1+wlen(text));	}
}

void pshow(lama* pos,lama len,const char* text)
{
	if (len > 0)
	{
		lama o = *pos;		// pos est un tableau de position des mots
		const char c = *(text+o); // les positions des mots sont relative au début de la ligne , text est l'@ du premier char de la ligne
		if(!( c ==  '\r' || c == '\n' || c == 0))
			{	wshow(text+o);	pshow(++pos,--len,text);	}
	}
}

const char* split(lama* to_fill,const char* text,lama offset)
{
	const char c = *text;
	if (c == '\r'|| c == '\n'|| c == 0) return ( c == '\r' ) ? ++text : text;
	else
		{
		  lama l = 1+wlen(text);
		  if (l > 1) *to_fill = offset;
		  offset += l;
		  text += l;
		  return split(++to_fill,text,offset);
		}
}

/*-------------- Request --------------*/

void fshow(line* lin,const char* text)
{
	if (lin)
		{
			printf("\n\tLigne n°%d\n\tNombre de mots : %ld\n\t",lin->rank,lin->nb_words);
			pshow(lin->pos,lin->nb_words,text);
			if (!(lin->next)) fshow(lin->next,lin->end+1);
			else fshow(lin->next,lin->next->start);
		}
}

line* dissector(const char* req,lama rank)
{
	line* newline = NULL;
	const char c = *req;
	if (!(c == '\r'|| c == '\n'|| c == 0))
		{
			newline = malloc(sizeof(line));
			newline->start = req;
			newline->rank = rank++;

			newline->nb_words = llen(req);
			newline->pos = malloc(newline->nb_words*sizeof(lama));
			newline->end = split(newline->pos,req,0);
			newline->next = dissector(newline->end+1,rank);
		}
	return newline;
}

void clearector(line* lin)
{
	if ( lin )
		{
			if ( lin->pos ) {	free(lin->pos); lin->pos = NULL;	}
			clearector(lin->next);
			free(lin);
		}
}



//Alloc suffisant nécessaire AVANT d'appeler la fonction
void imerge(lama* to_fill,lama* left,lama lsize,lama* right,lama rsize,const lama offset)
{
	if (lsize > 0)
		{	*to_fill = *left;	imerge(++to_fill,++left,--lsize,right,rsize,offset);	}
	else if (rsize > 0)
		{	*to_fill = (*right)+offset;	imerge(++to_fill,NULL,0,++right,--rsize,offset);	}
}

void pprank(line* lin,char value)
{
	if (lin)
		{	lin->rank += value;	pprank(lin->next,value);	}
}

//Retourne NULL si la ligne n'est pas joignable
line* reach(line* lin,lama rank)
{
	return (lin != NULL && lin->rank == rank) ? lin : ((lin == NULL || lin->rank > rank ) ? NULL : reach(lin->next,rank));
}


//Repose sur reach, imerge , pprank
void merge(line* lin,lama rank)
{
	lin = reach(lin,rank);
	if (lin && lin->next)
		{
			lama newsize = lin->nb_words + lin->next->nb_words;
			lama* newpos = malloc(newsize*sizeof(lama));
			line* nex = lin->next;

			imerge(newpos,lin->pos,lin->nb_words,nex->pos,nex->nb_words,(lama)(nex->start-lin->start));

			lin->pos = newpos;
			lin->nb_words = newsize;
			forget(&lin,nex->rank);
		}
}

// En cas de forget en tête il faut bien penser à actualiser l'origine , sinon l'affichage est decale
void forget(line** lin,lama rank)
{
	line* cursor = *lin;
	if (rank >= cursor->rank)
		 cursor = (rank == cursor->rank) ? cursor : reach(*lin,rank-1);
	else cursor = NULL;

	if (cursor)
	{
		line* tmp;
		if (rank == (*lin)->rank)
			{
				tmp = cursor;
				*lin = cursor->next;
			}
		else
			{
				tmp = cursor->next;
				if (tmp) cursor->next = tmp->next;
			}
		pprank(tmp,-1);
		free(tmp->pos);
		free(tmp);
	}
}
