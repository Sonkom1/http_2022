#include "../includes/syntax.h"

/* ----------------------------------ACCEPT_CHARSET_HEADER---------------------------------- */


//charset = token
const_p charset (const_p s)
{ return token(s); }

//charset_or_asterisk : ( charset / "*" )
const_p charset_or_asterisk (const_p s)
{ return ruleor(NULL, charset, "*", NULL, s); }

//_charset_maybe_weight = ( ( charset / "*" ) [ weight ] )
const_p _charset_maybe_weight (const_p s)
{ return rulefac(NULL, charset_or_asterisk, NULL, weight, s); }

//ows_and_charset_maybe_weight = OWS ( ( charset / "*" ) [ weight ] )
const_p ows_and_charset_maybe_weight (const_p s)
{ return rulecat(NULL, ows, NULL, _charset_maybe_weight, s); }

//single_part2_accept_charset = OWS "," [ OWS ( ( charset / "*" ) [ weight ] ) ]
const_p single_part2_accept_charset (const_p s)
{ return rulefac(NULL, ows_and_comma, NULL, ows_and_charset_maybe_weight, s); }

//_part1_accept_charset = * ( "," OWS ) ( ( charset / "*" ) [ weight ] )
const_p _part1_accept_charset (const_p s)
{ return rulecat(NULL, endless_comma_and_ows, NULL, _charset_maybe_weight, s); }

//_part2_accept_charset = * ( OWS "," [ OWS ( ( charset / "*" ) [ weight ] ) ] )
const_p _part2_accept_charset (const_p s)
{ return rulestar(NULL, single_part2_accept_charset, 0,INF,s); }

//Accept-Charset : * ( "," OWS ) ( ( charset / "*" ) [ weight ] ) * ( OWS "," [ OWS ( ( charset / "*" ) [ weight ] ) ] )
const_p accept_charset (const_p s)
{ return rulecat(NULL, _part1_accept_charset, NULL, _part2_accept_charset, s); }

//Accept-Charset-header : "Accept-Charset" ":" OWS Accept-Charset OWS
const_p accept_charset_header (const_p s) {
  const_p cursor = rulecat("Accept-Charset:", NULL, NULL, ows, s);
  if (cursor != NULL) {
    cursor = rulecat(NULL, accept_charset, NULL, ows, cursor);
  }
  return cursor;
}
