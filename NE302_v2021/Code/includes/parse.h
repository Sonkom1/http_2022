#include <stdio.h>
#include <stdlib.h>

typedef struct line line;
typedef size_t lama;
struct line
{
	const char* start;
	const char* end;

	lama nb_words;
	lama* pos;
	unsigned char rank;
	line* next;
};

#ifdef tst
	const char* req = "GET HTTP 1.0\r\nURL www.toto.com\r\nPOST HTTP 2.0\r\nURL www.tata.com\r\n\r\n";
#endif

/*-------------- Word & Line --------------*/
lama wlen(const char* text);
lama llen(const char* text);
void wshow(const char* text);
void lshow(const char* text);
void pshow(lama* pos,lama len,const char* text);
const char* split(lama* to_fill,const char* text,lama offset);

/*-------------- Request --------------*/
line* reach(line* lin,lama rank);
line* dissector(const char* req,lama rank);
void clearector(line* lin);
void fshow(line* lin,const char* text);
void imerge(lama* to_fill,lama* left,lama lsize,lama* right,lama rsize,const lama offset);
void pprank(line* lin,char value);
void merge(line* lin,lama rank);
void forget(line** lin,lama rank);
