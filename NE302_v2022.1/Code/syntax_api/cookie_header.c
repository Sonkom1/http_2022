#include "syntax.h"

/*     cookie-header =    "Cookie:" OWS cookie-string OWS
v        Cookie-string =    cookie-pair * ( ";" SP cookie-pair )
v            cookie-pair =    cookie-name "=" cookie-value
v                cookie-name =    token
v                cookie-value =    ( DQUOTE * cookie-octet DQUOTE ) / * cookie-octet
v                    cookie-octet =    %x21 / %x23-2B / %x2D-3A / %x3C-5B / %x5D-7E

*/

//cookie_pair_split sert pour créer les nodes. en effet, chaque cookie-pair est séparé par '; ', et on a besoin de ce séparateur lorqu'on appelle new_leaf_sp
const_p cookie_pair_split(const_p s)
{
    return ruleword("; ",s);
}

//x21 = 33 '!' (pas besoin de fonction)
//x23-2b = 35 '#' - 43 '+'
const_p _x23_2b(const_p s)
{ return (( *s >= 35 && *s <= 43) ? ++s : NULL);}
//x2d-3a = 45 '-' - 58 ':'
const_p _x2d_3a(const_p s)
{ return (( *s >= 45 && *s <= 58) ? ++s : NULL);}
//x3c-5b = 60 '<' - 91 '['
const_p _x3c_5b(const_p s)
{ return (( *s >= 60 && *s <= 91) ? ++s : NULL);}
//x5d-7e = 93 ']' - 126 '~'
const_p _x5d_7e(const_p s)
{ return (( *s >= 93 && *s <= 126) ? ++s : NULL);}

//cookie-octet =    %x21 / %x23-2B / %x2D-3A / %x3C-5B / %x5D-7E
const_p cookie_octet(const_p s)
{
    const_p cursor = s;
    cursor = ruleor("!",NULL,NULL,_x23_2b,s);
    if (cursor == NULL)
      {
        cursor = ruleor(NULL,_x2d_3a,NULL,_x3c_5b,s);
        if (cursor == NULL)
          {
            cursor = _x5d_7e(s);
          }
      }
    return cursor;
}

//_star_cookie_octet = * cookie-octet
const_p _star_cookie_octet(const_p s)
{
    const_p cursor = rulestar(NULL,cookie_octet,0,INF,s);
    return cursor;
}

//_1cookie_value = ( DQUOTE * cookie-octet DQUOTE )
const_p _1cookie_value(const_p s)
{
    const_p cursor = rulecat(NULL,dquote,NULL,_star_cookie_octet,s);
    if (cursor !=NULL)
      {
        cursor = dquote(cursor);
      }
    return cursor;
}

//cookie-value =    ( DQUOTE * cookie-octet DQUOTE ) / * cookie-octet
const_p cookie_value(const_p s)
{
    const_p cursor = ruleor(NULL,_1cookie_value,NULL,_star_cookie_octet,s);
    return cursor;
}

//cookie-name =   token
const_p cookie_name(const_p s)
{
    const_p cursor = token(s);
    return cursor;
}

//cookie-pair =    cookie-name "=" cookie-value
const_p cookie_pair(const_p s)
{
    const_p cursor = rulecat(NULL,cookie_name,"=",NULL,s);
    if (cursor !=NULL)
      {
        cursor = cookie_value(cursor);
      }
    return cursor;
}

//_1cookie_string =( ";" SP cookie-pair )
const_p _1cookie_string(const_p s)
{
    const_p cursor = rulecat(";",NULL,NULL,sp,s);
    if (cursor !=NULL)
      {
        cursor = cookie_pair(cursor);
      }
    return cursor;
}
//_star_cookie_string = * ( ";" SP cookie-pair )
const_p _star_1cookie_string(const_p s)
{
    const_p cursor = rulestar(NULL,_1cookie_string, 0, INF,s);
    return cursor;
}

//Cookie-string =    cookie-pair * ( ";" SP cookie-pair )
const_p cookie_string(const_p s)
{
    const_p cursor = rulecat(NULL,cookie_pair,NULL,_star_1cookie_string,s);
    return cursor;
}

//cookie-header =    "Cookie:" OWS cookie-string OWS
//content-type-header =    "Content-Type" ":" OWS Content-Type OWS
const_p cookie_header(const_p s)
{
    const_p cursor = rulecase("cookie", s);
    if (cursor != NULL)
    {
        cursor = rulecat(":", NULL, NULL, ows, cursor);
        if (cursor != NULL)
        {
            cursor = rulecat(NULL, cookie_string, NULL, ows, cursor);
        }
    }
    return cursor;
}
