#include "api.h"

_Token *api_list = NULL;

void *getRootTree()
{ return (void*)r00t;}


void fill_api_chained_list(node* to_fill){
  _Token *fill, *buffer;
  if(api_list == NULL) {
    api_list = (_Token *)malloc(sizeof(_Token));
    api_list->node = to_fill;
    api_list->next = NULL;
  } else {
    buffer = api_list;
    while (buffer != NULL) {
      fill = buffer;
      buffer = buffer->next;
    }
    fill->next = (_Token *)malloc(sizeof(_Token));
    fill = fill->next;
    fill->node = to_fill;
    fill->next = NULL;
  }
}

_Token *searchTree(void *start,char *name) {
  node* search = start;
  if (search == NULL) search = getRootTree();

  if ((search->tag != NULL) && (strcmp(search->tag,name) == 0)) fill_api_chained_list(search);

  if (search->left != NULL) searchTree(search->left, name);
  if (search->right != NULL) searchTree(search->right, name);
  if (search->mid != NULL) searchTree(search->mid, name);
  if (search->next != NULL) searchTree(search->next, name);

  return api_list;
}

char *getElementTag(void *node,int *len)
{ return (char*)((leaf*)node)->tag; }

char *getElementValue(void *node,int *len)
{
  *len = ((leaf*)node)->len;
  return (char*)((leaf*)node)->value;
}

void purgeElement(_Token **r)
{
    if (r != NULL){
        if (*r!=NULL){
            if ((*r)->next == NULL){
                free(*r);
            }else{
                purgeElement(&((*r)->next));
                free(*r);
            }
        }
    }
}

void purgeTree(void *root) { clear_node(root);  }

int parseur(char *req, int len)
{  r00t = http_message_node(req); return (r00t) ? 1 : 0; }
