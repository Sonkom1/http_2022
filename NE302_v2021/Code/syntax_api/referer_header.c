#include "../includes/syntax.h"

/* ----------------------------------REFERER---------------------------------- */

//segment-nz : 1* pchar
const_p segment_nz (const_p s)
{ return rulestar(NULL,pchar,1,INF,s); }

//_unres_or_pct_or_sub = unreserved / pct-encoded / sub-delims
const_p _unres_or_pct_or_sub (const_p s) {
  const_p cursor = unreserved(s);
  if (cursor == NULL) cursor = ruleor(NULL,pct_encoded,NULL,sub_delim,s);
  return cursor;
}

//_single_segment_nz_nc = _unres_or_pct_or_sub / "@"
const_p _single_segment_nz_nc (const_p s) {
  const_p cursor = _unres_or_pct_or_sub(s);
  if (cursor == NULL) cursor = rulechar("@",s);
  return cursor;
}

//segment-nz-nc : 1* ( unreserved / pct-encoded / sub-delims / "@" )
const_p segment_nz_nc(const_p s)
{ return rulestar(NULL,_single_segment_nz_nc,1,INF,s); }


//_path_segment = "/" segment
const_p _path_segment (const_p s)
{ return rulecat("/",NULL,NULL,segment,s); }

//path-abempty : * ( "/" segment )
const_p path_abempty (const_p s)
{ return rulestar(NULL,_path_segment,0,INF,s); }

//path-noscheme : segment-nz-nc * ( "/" segment )
const_p path_noscheme (const_p s)
{ return rulecat(NULL,segment_nz_nc,NULL,path_abempty,s); }

//path-empty : ""
const_p path_empty(const_p s)
{ return ruleword("",s); }

//path-rootless : segment-nz * ( "/" segment )
const_p path_rootless (const_p s)
{ return rulecat(NULL,segment_nz,NULL,path_abempty,s); }

//path-absolute : "/" [ segment-nz * ( "/" segment ) ]
const_p path_absolute (const_p s) {
  const_p cursor = rulechar("/", s);
  s = cursor;
  if (cursor != NULL) cursor = path_rootless(cursor);
  return ((cursor == NULL) ? s : cursor);
}


//_userinfo = _unres_or_pct_or_sub / ":"
const_p _userinfo (const_p s)
{ return ruleor(NULL, _unres_or_pct_or_sub, "/", NULL, s); }

//userinfo : * ( unreserved / pct-encoded / sub-delims / ":" )
const_p userinfo (const_p s)
{ return rulestar(NULL,_userinfo,0,INF,s); }

//authority : [ userinfo "@" ] host [ ":" port ]
const_p authority (const_p s){
  const_p cursor = rulecat(NULL, userinfo, "@", NULL, s);
  if (cursor == NULL) cursor = s;

  cursor = host(cursor);
  if (cursor != NULL) s = cursor;

  cursor = rulecat(":", NULL, NULL, port, cursor);
  return ((cursor == NULL) ? s : cursor);
}


const_p _path_abempty_or_absolute (const_p s)
{ return ruleor(NULL, path_abempty, NULL, path_absolute, s); }

const_p _path_rootless_or_empty (const_p s)
{ return ruleor(NULL, path_rootless, NULL, path_empty, s); }

const_p _path_noscheme_or_empty (const_p s)
{ return ruleor(NULL, path_noscheme, NULL, path_empty, s); }

//hier-part : "//" authority path-abempty / path-absolute / path-rootless / path-empty
const_p hier_part (const_p s) {
  const_p cursor = rulecat("//", NULL, NULL, authority, s);
  if (cursor != NULL) cursor = ruleor(NULL, _path_abempty_or_absolute, NULL, _path_rootless_or_empty, cursor);
  return cursor;
}

//relative-part : "//" authority path-abempty / path-absolute / path-noscheme / path-empty
const_p relative_part (const_p s) {
  const_p cursor = rulecat("//", NULL, NULL, authority, s);
  if (cursor != NULL) cursor = ruleor(NULL, _path_abempty_or_absolute, NULL, _path_noscheme_or_empty, cursor);
  return cursor;
}

//_1scheme = ALPHA / DIGIT / "+" / "-" / "."
const_p _1scheme (const_p s){
  const_p cursor = alpha(s);
  if (cursor == NULL) {
    cursor = digit(s);
    if (cursor == NULL) {
      cursor = rulechar("+", s);
      if (cursor == NULL) {
        cursor = rulechar("-", s);
        if (cursor == NULL) {
          cursor = rulechar(".", s);
        }
      }
    }
  }
  return cursor;
}

//_2scheme = * ( ALPHA / DIGIT / "+" / "-" / "." )
const_p _2scheme (const_p s)
{ return rulestar(NULL,_1scheme,0,INF,s); }

//scheme : ALPHA * ( ALPHA / DIGIT / "+" / "-" / "." )
const_p scheme (const_p s)
{ return rulecat(NULL, alpha, NULL, _2scheme, s); }


const_p question_mark_query(const_p s)
{ return rulecat("?", NULL, NULL, query, s); }

//absolute-URI : scheme ":" hier-part [ "?" query ]
const_p absolute_uri(const_p s){
  const_p cursor = rulecat(NULL, scheme, ":", NULL, s);
  if (cursor != NULL) {
    cursor = hier_part(cursor);
    if (cursor != NULL) {
      s = cursor;
      cursor = question_mark_query(s);
      if (cursor == NULL) cursor = s;
    }
  }
  return cursor;
}

//partial-URI : relative-part [ "?" query ]
const_p partial_uri (const_p s) {
  const_p cursor = relative_part(s);
  if (cursor != NULL) {
    s = cursor;
    cursor = question_mark_query(s);
    if (cursor == NULL) cursor = s;
  }
  return cursor;
}

//Referer-header : "Referer" ":" OWS Referer OWS
const_p referer_header (const_p s) {
  const_p cursor = rulecat("Referer:", NULL, NULL, ows, s);
  if (cursor != NULL) {
    cursor = rulecat(NULL, referer, NULL, ows, cursor);
  }
  return cursor;
}

//Referer : absolute-URI / partial-URI
const_p referer (const_p s)
{ return ruleor(NULL, absolute_uri, NULL, partial_uri, s); }
