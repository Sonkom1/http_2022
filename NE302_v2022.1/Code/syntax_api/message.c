#include "syntax.h"

const_p nomDuHeader(const_p s)
{
    int i=0;
    int j = 0;
    char * tag = NULL;
    while ((s[++i] != ':') && (s[i] != '\0'))
    {
    }
    if (s[i] != ':') {printf("Erreur: etiquette introuvable.\n");exit(1);}
    tag = malloc((i+1)*sizeof(char));
    for (j=0;j<i;j++)
    {
        tag[j] = *s;
        s++;
    }
    return (const_p)tag;
}
const_p implemented_only(const_p s)
{
    const_p cursor = NULL;
  if (s && *s != 0)
  {
    if (cursor == NULL) cursor = connection_header(s);
    if (cursor == NULL) cursor = content_length_header(s);
    if (cursor == NULL) cursor = content_type_header(s);
    if (cursor == NULL) cursor = cookie_header(s);
    if (cursor == NULL) cursor = transfer_encoding_header(s);
    if (cursor == NULL) cursor = expect_header(s);
    if (cursor == NULL) cursor = host_header(s);
    if (cursor != NULL) cursor = crlf(cursor);
  }
  return cursor;
}

//message-body : * OCTET
const_p message_body(const_p s)
{ return rulestar(NULL,octet,0,INF,s);  }

//obs-fold :  CRLF 1* ( SP / HTAB )
const_p obs_fold(const_p s)
{
    const_p cursor = crlf(s);
    if (cursor != NULL) cursor = rulestar(NULL,wsp,1,INF,cursor);
    return cursor;
}

//field-name : token
const_p field_name(const_p s)
{ return token(s); }

//field-vchar = VCHAR / obs-text;
const_p field_vchar(const_p s)
{  return ruleor(NULL,vchar,NULL,obs_text,s); }

//_fied_content : 1* ( SP / HTAB ) field-vchar
const_p _field_content(const_p s)
{
  const_p cursor = rulestar(NULL,wsp,1,INF,s);
  if (cursor != NULL) cursor = field_vchar(cursor);
  return cursor;
}

//field-content = field-vchar [_field_content]
const_p field_content(const_p s)
{  return rulefac(NULL,field_vchar,NULL,_field_content,s); }

//_field-value = field-content / obs-fold
const_p _field_value(const_p s)
{  return ruleor(NULL,field_content,NULL,obs_fold,s); }

//field-value = * ( _field_value)
const_p field_value(const_p s)
{ return rulestar(NULL,_field_value,0,INF,s); }

//_other :  field-name ":" OWS field-value OWS
const_p _other(const_p s)
{
  const_p cursor = rulecat(NULL,field_name,":",NULL,s);
  if (cursor != NULL)
  {
    cursor = rulecat(NULL,ows,NULL,field_value,cursor);
    if (cursor != NULL)
      {
        cursor = ows(cursor);
      }
  }
  return cursor;
}

//Connection-header / Content-Length-header / Content-Type-header / Cookie-header / Transfer-Encoding-header / Expect-header / Host-header / ( field-name ":" OWS field-value OWS )
const_p _header_fields(const_p s)
{
  const_p cursor = NULL;
  if (s && *s != 0)
  {
    if (cursor == NULL) cursor = connection_header(s);
    if (cursor == NULL) cursor = content_length_header(s);
    if (cursor == NULL) cursor = content_type_header(s);
    if (cursor == NULL) cursor = cookie_header(s);
    if (cursor == NULL) cursor = transfer_encoding_header(s);
    if (cursor == NULL) cursor = expect_header(s);
    if (cursor == NULL) cursor = host_header(s);
    if (cursor == NULL) cursor = _other(s);
    if (cursor != NULL) cursor = crlf(cursor);
  }
  return cursor;
}


//start-line :   request-line / status-line
const_p start_line(const_p s)
{  return ruleor(NULL,status_line,NULL,request_line,s); }

//HTTP-message = start-line * ( header-field CRLF ) CRLF [ message-body ]
const_p http_message(const_p s)
{
  const_p cursor = start_line(s);
  if (cursor != NULL) cursor = rulestar(NULL,_header_fields,0,INF,cursor);
  if (cursor != NULL) cursor = rulefac(NULL,crlf,NULL,message_body,cursor);
  return cursor;
}
