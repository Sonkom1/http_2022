#include "syntax.h"
/*
request-line = method SP request-target SP HTTP-version CRLF
method = token
request-target = origin-form
  HTTP-version = HTTP-name "/" DIGIT "." DIGIT
  origin-form = absolute-path [ "?" query ]
  absolute-path = 1* ( "/" segment )
  query = * ( pchar / "/" / "?" )
    HTTP-name = %x48.54.54.50
    segment = * pchar
    pchar = unreserved / pct-encoded / sub-delims / ":" / "@"
      unreserved = ALPHA / DIGIT / "-" / "." / "_" / "~"
      pct-encoded = "%" HEXDIG HEXDIG
      sub-delims = "!" / "$" / "&" / "'" / "(" / ")" / "*" / "+" / "," / ";" / "="
*/

//sub-delims = "!" / "$" / "&" / "'" / "(" / ")" / "*" / "+" / "," / ";" / "="
const_p sub_delim(const_p s)
{
    const_p cursor = s;
    cursor = ruleor("!",NULL,"$",NULL,s);
    if (cursor == NULL)
      {
        cursor = ruleor("&",NULL,"'",NULL,s);
        if (cursor == NULL)
          {
            cursor = ruleor("(",NULL,")",NULL,s);
            if (cursor == NULL)
              {
                cursor = ruleor("*",NULL,"+",NULL,s);
                if (cursor == NULL)
                  {
                    cursor = ruleor(",",NULL,";",NULL,s);
                    if (cursor == NULL)
                      {
                        cursor = rulechar("=",s);
                      }
                  }
              }
          }
      }
    return cursor;
}

// pct-encoded = "%" HEXDIG HEXDIG
const_p pct_encoded(const_p s)
{
    const_p cursor = rulechar("%",s);
    if (cursor != NULL)
      {
        cursor = rulecat(NULL,hex,NULL,hex,cursor);
      }
    return cursor;
}

// unreserved = ALPHA / DIGIT / "-" / "." / "_" / "~"
const_p unreserved(const_p s)
{
    const_p cursor = ruleor(NULL,alpha,NULL,digit,s);
    if ( cursor == NULL)
      {
        cursor = ruleor("-",NULL,".",NULL,s);
        if (cursor == NULL)
          {
            cursor = ruleor("_",NULL,"~",NULL,s);
          }
      }
    return cursor;
}

//pchar = unreserved / pct-encoded / sub-delims / ":" / "@"
const_p pchar(const_p s)
{
    const_p cursor = ruleor(NULL,unreserved,NULL,pct_encoded,s);
    if ( cursor == NULL)
      {
        cursor = ruleor(NULL,sub_delim,":",NULL,s);
        if (cursor == NULL)
          {
            cursor = rulechar("@",s);
          }
      }
    return cursor;
}

//segment = * pchar
const_p segment(const_p s)
{
    const_p cursor = rulestar(NULL,pchar,0,INF,s);
    return cursor;
}

//tchar : "!" / "#" / "$" / "%" / "&" / "'" / "*" / "+" / "-" / "." / "^" / "_" / "`" / "|" / "~" / DIGIT / ALPHA
const_p tchar(const_p s)
{
  const_p cursor = ruleor("!",NULL,"#",NULL,s);
  if ( cursor == NULL)
    {
      cursor = ruleor("$",NULL,"%",NULL,s);
      if ( cursor == NULL)
        {
          cursor = ruleor("&",NULL,"'",NULL,s);
          if ( cursor == NULL)
            {
              cursor = ruleor("*",NULL,"+",NULL,s);
              if ( cursor == NULL)
                {
                  cursor = ruleor("-",NULL,".",NULL,s);
                  if ( cursor == NULL)
                    {
                      cursor = ruleor("^",NULL,"_",NULL,s);
                      if ( cursor == NULL)
                        {
                          cursor = ruleor("`",NULL,"|",NULL,s);
                          if ( cursor == NULL)
                            {
                              cursor = ruleor("~" ,NULL,NULL,digit,s);
                              if ( cursor == NULL)
                                {
                                  cursor = alpha(s);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
  return cursor;
}

//HTTP-name = %x48.54.54.50
const_p http_name(const_p s)
{
    const_p cursor = ruleword("HTTP",s);
    return cursor;
}

//_query = pchar / "/" / "?"
const_p _query(const_p s)
{
    const_p cursor = rulechar("/",s);
    if ( cursor == NULL)
      {
        cursor = ruleor("?",NULL,NULL,pchar,s);
      }
    return cursor;
}
//query = * ( _query )
const_p query(const_p s)
{
  const_p cursor = rulestar(NULL,_query,0,INF,s);
  return cursor;
}

//_absolute-path =  "/" segment
const_p _absolute_path(const_p s)
{
    const_p cursor = rulecat("/",NULL,NULL,segment,s);
    return cursor;
}

//absolute-path = 1* ( _absolute-path )
const_p absolute_path(const_p s)
{
  const_p cursor = rulestar(NULL,_absolute_path,0,INF,s);
  return cursor;
}

//_origin_form = [ "?" query ]
const_p _origin_form(const_p s)
{
    const_p cursor = rulecat("?",NULL,NULL,query,s);
    return cursor;
}

//origin-form = absolute-path [_origin_form]
const_p origin_form(const_p s)
{
    const_p cursor = rulefac(NULL,absolute_path,NULL,_origin_form,s);
    return cursor;
}

//token : 1* tchar
const_p token(const_p s)
{ return rulestar(NULL,tchar,1,INF,s); }


//HTTP-version = HTTP-name "/" DIGIT "." DIGIT
const_p http_version(const_p s)
{
    const_p cursor = rulecat(NULL,http_name,"/",NULL,s);
    if ( cursor != NULL)
      {
        cursor = rulecat(NULL,digit,".",NULL,cursor);
        if ( cursor != NULL)
        {
          cursor = digit(cursor);
        }
      }
    return cursor;
}

//request-target = origin-form
const_p request_target(const_p s)
{
  const_p cursor = origin_form(s);
  return cursor;
}

//method = token
const_p method(const_p s)
{
  const_p cursor = token(s);
  return cursor;
}

//request-line = method SP request-target SP HTTP-version CRLF
const_p request_line(const_p s)
{
    const_p cursor = rulecat(NULL,method,NULL,sp,s);
    if ( cursor != NULL)
      {
        cursor = rulecat(NULL,request_target,NULL,sp,cursor);
        if ( cursor != NULL)
          {
            cursor = rulecat(NULL,http_version,NULL,crlf,cursor);
          }
      }
    return cursor;
}
