#include "../includes/request_line.h"
#include <string.h>
#include <assert.h>
#include "../includes/zlib.h"
#define SET_BINARY_MODE(file)
#define CHUNK 16384
//Z_DEFAULT_COMPRESSION
//https://zlib.net/zlib_how.html
int def(FILE *source, FILE *dest, int level)
{
  int ret, flush;
  unsigned have;
  z_stream strm;
  unsigned char in[CHUNK];
  unsigned char out[CHUNK];

  strm.zalloc = Z_NULL;
  strm.zfree = Z_NULL;
  strm.opaque = Z_NULL;
  ret = deflateInit(&strm, level);
  if (ret != Z_OK)
      return ret;
  do {
      strm.avail_in = fread(in, 1, CHUNK, source);
      if (ferror(source))
        {
          (void)deflateEnd(&strm);
          return Z_ERRNO;
        }
    flush = feof(source) ? Z_FINISH : Z_NO_FLUSH;
    strm.next_in = in;
    do{
        strm.avail_out = CHUNK;
        strm.next_out = out;

        ret = deflate(&strm, flush);
        have = CHUNK - strm.avail_out;
        if (fwrite(out, 1, have, dest) != have || ferror(dest))
          {
            (void)deflateEnd(&strm);
            return Z_ERRNO;
          }

      } while (strm.avail_out == 0);
      assert(strm.avail_in == 0);
      } while (flush != Z_FINISH);
      assert(ret == Z_STREAM_END);
      (void)deflateEnd(&strm);
  return Z_OK;
}

int inf(FILE *source, FILE *dest)
{
  int ret;
  unsigned have;
  z_stream strm;
  unsigned char in[CHUNK];
  unsigned char out[CHUNK];
  strm.zalloc = Z_NULL;
  strm.zfree = Z_NULL;
  strm.opaque = Z_NULL;
  strm.avail_in = 0;
  strm.next_in = Z_NULL;
  ret = inflateInit(&strm);
  if (ret != Z_OK)
    return ret;
  do {
      strm.avail_in = fread(in, 1, CHUNK, source);
      if (ferror(source))
      {
        (void)inflateEnd(&strm);
        return Z_ERRNO;
      }
      if (strm.avail_in == 0)
          break;
      strm.next_in = in;
      do {
          strm.avail_out = CHUNK;
          strm.next_out = out;
          ret = inflate(&strm, Z_NO_FLUSH);
          assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
          switch (ret)
            {
              case Z_NEED_DICT:
                ret = Z_DATA_ERROR;     /* and fall through */
              case Z_DATA_ERROR:
              case Z_MEM_ERROR:
                (void)inflateEnd(&strm);
                return ret;
            }
            have = CHUNK - strm.avail_out;
            if (fwrite(out, 1, have, dest) != have || ferror(dest))
            {
              (void)inflateEnd(&strm);
              return Z_ERRNO;
            }
          } while (strm.avail_out == 0);
      } while (ret != Z_STREAM_END);
      (void)inflateEnd(&strm);
      return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

// ------------------------------------------------------ //
char* next_slash(char* filepath)
{
  if (!filepath) return NULL;
  char* c = filepath;
  while (*c && *(c++) != '/');
  return c;
}

char* previous_slash(char* filepath,char* root)
{
  char* c = filepath;
  if (*filepath == '/' && filepath != root) --c;
  while (c > root && *(c--) != '/');
  return c;
}

//malloc
char* path_segment(char* filepath)
{
  char* start = (*filepath == '/') ? ++filepath : filepath;
  char* end = next_slash(start);
  if (end == start) return NULL;

  char* to_return = malloc((1+end-start)*sizeof(char));
  strncpy(to_return,start,(end-start));
  to_return[end-start] = 0;

  return to_return;
}

char* get_last_slash(char* filepath)
{
  if (filepath == NULL) return NULL;
  char* cursor = filepath;
  while (*filepath)
  {
    if (*filepath == '/') cursor = filepath;
    filepath++;
  }
  return cursor;
}
// ------------------------------------------------------ //

//malloc
char* get_folder(char* filepath)
{
  if (filepath == NULL) return NULL;
  int l = get_last_slash(filepath)-filepath;
  if (*filepath != '/') l +=2;
  char* to_return = malloc((1+l)*sizeof(char));

  if (*filepath != '/')
  {
    to_return[0] = '.'; to_return[1] = '/';
    for (int i = 0; i < l-2 ; i++) to_return[i+2] = filepath[i];
  }

  else  {    for (int i = 0; i < l ; i++) to_return[i] = filepath[i]; }

  to_return[l] = 0;
  return to_return;
}

//malloc
char* get_filename(char* filepath)
{
  if (filepath == NULL) return NULL;
  char* to_return;
  char* c = get_last_slash(filepath);  c++;
  int l = 0;
  while (*c)
  {    c++;    l++;  }
  if (l == 0) return NULL;

  c = get_last_slash(filepath);  c++;
  to_return = malloc((1+l)*sizeof(char));
  strncpy(to_return,c,l);

  return to_return;
}

DIR* open_dir(char* foldername)
{
  DIR* to_return = NULL;
  if (foldername)
  {    to_return = opendir(foldername);  }
  return to_return;
}

void get_filetype(const char* actual_file,files* file)
{
  const char *magic_full;
  magic_t magic_cookie;

  magic_cookie = magic_open(MAGIC_MIME_TYPE);

  if (magic_cookie == NULL)
  {  printf("unable to initialize magic library\n");  }

  if (magic_load(magic_cookie, NULL) != 0)
  {
    printf("cannot load magic database - %s\n", magic_error(magic_cookie));
    magic_close(magic_cookie);
  }

  magic_full = magic_file(magic_cookie, actual_file);
  file->cookie = magic_cookie;
  file->magic = magic_full;
}

//renvoie NULL si path mène nulle part, une structure avec stream à NULL si fichier inexistant , une structure complète si le fichier existe
struct files* open_file(char* filepath)
{
  struct files* to_return = NULL;
  char* path = filepath;
  char* foldername = get_folder(filepath);
  char* filename = get_filename(filepath);

  to_return = malloc(sizeof(files));
  to_return->fullpath = path;
  to_return->stream = fopen(path,"rb");

  if (to_return->stream)
  {
    get_filetype(path,to_return);
  }
  else
  {
      to_return->magic = NULL;
      to_return->cookie = 0;
  }

  if (foldername) free(foldername);
  if (filename) free(filename);
  return to_return;
}

void close_file(files* to_close,int full)
{
  if (to_close)
  {
    if (full) free((void*)to_close->fullpath);
    if (to_close->stream) fclose(to_close->stream);
    if (to_close->cookie) magic_close(to_close->cookie);
    free(to_close);
  }
}

char* read_file(long int* len,char* type)
{
  Field** headers = get_head();
  char* file = headers[TARGET]->content;
  char* fileStr ;
  int counter = 0;
  files *file_open = open_file(file);
  while(!feof(file_open->stream)) {
    getc(file_open->stream);
    counter++;
  }
  rewind(file_open->stream);

  fileStr = (char*)malloc((counter+1)*sizeof(char));
  for (int i=0;i<counter;i++) {
      fileStr[i] = fgetc(file_open->stream);
  }
  fileStr[counter] = '\0';

  int size = strlen(file_open->magic);
  strncpy(type,file_open->magic,45);
  type[(size+1 < 46) ? size+1 : 45] = 0;

  close_file(file_open,0);
  *len = counter -1;
  return fileStr;
}


void show(files* file)
{
  if (file)
  {
    printf("fullpath: %s\n",file->fullpath);
    printf("filetype: %s\n",file->magic);
  }
}
