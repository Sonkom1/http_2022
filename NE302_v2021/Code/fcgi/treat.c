/*Petit memo:
comment tester le code:
    méthode 1:
    http://127.0.0.1:8080/ sur firefox (pas trop de feedback)

    méthode 2 (mieux car on voit la réponse du serveur ptn c trop beau):
        printf 'GET /get HTTP/1.1\r\nHost:127.0.0.1\r\n\r\n' | nc -C 127.0.0.1 8080
        printf 'GET HTTP/1.1\r\n AAAAA JE SUIS UNE REQUETE INCORRECTE AAAAA Host:127.0.0.1\r\n\r\n' | nc -C 127.0.0.1 8080

commande magique pour la compil:
    export LD_LIBRARY_PATH=.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
//#include "../includes/request_line.h"
#include "../includes/zlib.h"
#include "../includes/httpparser.h"
#include "../includes/syntax.h"
#include "../includes/ffcgi.h"
//#include "../includes/api.h"
#define TEST_REQUEST "GET / HTTP/1.1\r\nHost: 127.0.0.1:8080\r\n\r\n"

//Erreurs:
#define OK "200 OK"
#define BAD_REQUEST "400 Bad Request"
#define NOT_FOUND "404 Not Found"
#define LENGTH_REQUIRED "411 Length Required"
#define NOT_IMPLEMENTED "501 Not Implemented"
#define NOT_SUPPORTED "505 HTTP Version Not Supported"


//Misc
#define DATE_SIZE 80
#define MAX_RESPONSE_SIZE 1003000
#define MAX_HEADERS_SIZE 2000
#define MAX_PAYLOAD_SIZE 1000000
#define DEFAULT_CONTENT_TYPE "*/*"

//remarques:
//MAX_HEADERS_SIZE définit la longueur max de la string contenant *tous les headers à la suite*



int traiterRequete(message *req)
{
    message *rep;
    _Token *root;
    char *version, *payload, *headerString, *statusLine, *reponse;
    char * mthd = "valeurInitiale";
    long int longueurReponse = 0;
    char isParsed = 0;
    int sendContentType = 1;
    Field** headers;
    //char *payload;
    int errorCode = 200;
    int res;
    int keepAlive;
    int isPhp = 0;
    if ((rep=malloc(sizeof(message))) != NULL ) {


            /*
            -----------------PARSING---------------
            la chaine à traiter se trouve dans req->buf, sa longueur est req->len
            */
            printf("parsing...\n");

            if (!(res=parseur(req->buf,req->len))){
               printf("!! Echec lors du parsing\n");
               reponse = malloc(sizeof(char)*(strlen(BAD_REQUEST)+1));
               strncpy(reponse,BAD_REQUEST,1+strlen(BAD_REQUEST));
               errorCode = 400;
               version = (char*) malloc(9*sizeof(char));
               strncpy(version,"HTTP/1.0",1+strlen("HTTP/1.0"));
               version[strlen("HTTP/1.0")] = '\0';
            } else {
                printf("j'ai survécu au parsing !\n");
                isParsed = 1;
                root = getRootTree();
                /*
                ---------------VERIF SEMANTIQUE----------
                toutes les vérifications sot faites par check_request_line().
                */
                fill_field();
                headers = get_head();
                version=headers[HTTP_VERSION]->content;

                printf("\n--fetching ressource:--\n%s\n---\n",full_path());
                errorCode = check_request_line(headers,&isPhp);
                sendContentType = (isPhp==0) ? 1:0;
            }
            /*-------------*/
            /*
            --------------STATUS LINE---------------
            */
            statusLine = (char*) malloc(300*sizeof(char));
            strncpy(statusLine,version,strlen(version));
            statusLine[strlen(version)] = '\0';
            strcat(statusLine," ");
            switch (errorCode)
            {
                case 200: strcat(statusLine,OK); break;
                case 400: strcat(statusLine,BAD_REQUEST); break;
                case 404: strcat(statusLine,NOT_FOUND); break;
                case 411: strcat(statusLine,LENGTH_REQUIRED); break;
                case 501: strcat(statusLine,NOT_IMPLEMENTED); break;
                case 505: strcat(statusLine,NOT_SUPPORTED); break;
            }
            strcat(statusLine,"\r\n");
            /*-------------*/


            /*
            --------------BODY-----------
            */
            //ici, on met le contenu de la réponse (body) (fichier, etc peu importe) dans la string payload.
            //on définit également le content-type qui sera renvoyé. Pour ne pas renvoyer de content-type, sendContentType = 0.
            char contentTypeStr[45] = DEFAULT_CONTENT_TYPE;
            long int len = 0;
            if (errorCode == 200) {
                mthd = headers[METHOD]->content;
                if (strcmp(mthd,"POST")==0){
                    payload = "\0";
                }
                else {
                    payload =read_file(&len,contentTypeStr);
                }

                /*TEST DE read_file*/
                //le content_type est contenu dans contentTypeStr
            } else {
                payload = error_bodies(errorCode);
                strncpy(contentTypeStr,DEFAULT_CONTENT_TYPE,strlen(DEFAULT_CONTENT_TYPE));
                contentTypeStr[strlen(DEFAULT_CONTENT_TYPE)] = '\0';
            }


            /*
            --------------HEADERS-----------
            */
            /*gestion generale*/
            headerString = (char *)malloc(MAX_HEADERS_SIZE*sizeof(char));
            headerString[0] = '\0';

            /*DATE*/
            time_t timestamp = time(NULL);
            struct tm *pTime = localtime(&timestamp);
            char dateHdr [DATE_SIZE];
            strftime(dateHdr, DATE_SIZE, "Date: %a, %d %b %G %T %Z\r\n",pTime);
            strcat(headerString,dateHdr);
            /*-------------*/

            /*CONTENT-TYPE*/
            printf("aled il se passe quoi %d,%d\n",sendContentType,isPhp);
            if (isPhp == -1) sendContentType= 1;
            if (sendContentType == 1) {
                if (isPhp == 0) {
                    char contentTypeHdr[64] = "Content-type: ";
                    strcat(contentTypeHdr, contentTypeStr);
                    strcat(contentTypeHdr, "\r\n");
                    strcat(headerString, contentTypeHdr);
                } else if (isPhp == -1) {
                    printf("CSS!!!!\n");
                    char contentTypeHdr[64] = "Content-type: text/css\r\n";
                    strcat(headerString, contentTypeHdr);
                }
            }
            /*-------------*/

            /*CONNECTION*/
            /*rappel de la RFC 7230:
            les vérifications doivent etre effectuées dans cet ordre:
            -si on a conenction: close, on ferme la connexion  (après réponse)
            -sinon, si on a HTTP 1.1, on garde la connexion
            -sinon, si on a HTTP 1.0 et connection: keep-alive, on garde la connexion
            -sinon, on ferme la connexion (après réponse)
            rq: le header n'est obligatoire dans la réponse qu'en cas de close.
            ici, on ne renvoie un header connection que si on ferme la connection
            */
            char * ctn = "";
            if (isParsed) {
                ctn = headers[CONNECTION]->content;
            }
            if (strcmp(ctn,"close")==0) {
                    keepAlive = 0;
                    strcat(headerString, "Connection: close\r\n");
                    printf("la connection SERA coupée immédiatement");
            } else if (strcmp(version,"HTTP/1.1")==0){
                keepAlive = 1;
                printf("HTTP 1.1, la connection NE SERA PAS coupée immédiatement");
            } else if ((strcmp(version,"HTTP/1.0")==0) && (strcmp(ctn,"keep-alive")==0)) {
                keepAlive = 1;
                printf("HTTP 1.0 et keep-alive, la connection NE SERA PAS coupée immédiatement");
            } else {
                keepAlive = 0;
                printf("HTTP 1.0, la connection SERA coupée immédiatement");
                strcat(headerString, "Connection: close\r\n");
            }
            /*-------------*/
            
            /*CONTENT-LENGTH*/
            if (isPhp<=0) { 
                if (len > 0) {
                    char contentLengthHdr[26] = "Content-length: ";
                    char contentLengthStr[6];
                    sprintf(contentLengthStr, "%ld", len);
                    strcat(contentLengthHdr, contentLengthStr);
                    strcat(contentLengthHdr, "\r\n");
                    strcat(headerString, contentLengthHdr);
                }
            } else if (isPhp == 1){
                int lenPHP = 0;
                payload = php(&lenPHP);
                char contentLengthHdr[26] = "Content-length: ";
                char contentLengthStr[6];
                sprintf(contentLengthStr, "%d", lenPHP);
                strcat(contentLengthHdr, contentLengthStr);
                strcat(contentLengthHdr, "\r\n");
                strcat(headerString, contentLengthHdr);
            }
            /*-------------*/
            
            
            /*
            -----------ON RECOLLE LES MORCEAUX
            */
            reponse = malloc(MAX_RESPONSE_SIZE * sizeof(char));
            reponse[0] = '\0';
            strcat(reponse,statusLine);
            strcat(reponse,headerString);
            strcat(reponse,"\r\n");
            longueurReponse = strlen(reponse);

            if (strcmp(mthd,"GET")==0) {
                int finReponse = strlen(reponse);
                if (len <= MAX_RESPONSE_SIZE) {
                  for (int i=0; i < len; i++) {
                    reponse[finReponse + i] = payload[i];
                  }
                }
                longueurReponse += len;
            }

            /*failsafe*/
            if (reponse == NULL) {
                printf("! ! ! ! ! inattendu, défaut à OK\n");
                reponse = malloc(strlen(OK));
                strncpy(reponse,OK,1+strlen(OK));
            }



            /*
            --------------ENVOI DEMAX_RESPONSE LA REPONSE--------------
            */

            printf("\n\n\nREPONSE ENVOYEE:\n");
            for (int i=0; i<longueurReponse; i++){
              printf("%c",reponse[i]);
            }

			rep->len= longueurReponse;
			/*-------------*/

            writeDirectClient(req->clientId,reponse,longueurReponse);
            endWriteDirectClient(req->clientId);


			/*
			-------------GESTION DE LA CONNECTION----------
			*/
			//pour fermer la connection. En réalité, il ferme la connection quand même si cette ligne n'est pas appelée donc jsp bro
			if (keepAlive == 0) //cet entier contient la bonne valeur, mais le comportement lui ne s'adapte pas à cela...
			{
			printf("\n\n##### tentative de fermeture de la connection #####\n\n");
			requestShutdownSocket(rep->clientId);
			}
			/*-------------*/
			if (isParsed) {
			    purgeTree(root);
			}
		}
		else {return 1;}
	return 0;
}
