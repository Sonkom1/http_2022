#include "../includes/tree.h"

#define NB_TAG 13
#define TO_IMP 60

node* (*_node[])(const_p) ={&connection_node,&content_length_node,&content_type_node,\
				 &cookie_node,&transfer_encoding_node,\
				 &host_node,&accept_node,&accept_charset_node,\
				 &accept_encoding_node,&accept_language_node,\
				 &referer_node,&user_agent_node,&expect_node};

Rule with_dots[]={&connection_header,&content_length_header,&content_type_header,\
				 &cookie_header,&transfer_encoding_header,\
				 &host_header,&accept_header,&accept_charset_header,\
				 &accept_encoding_header,&accept_language_header,\
				 &referer_header,&user_agent_header,&expect_header};

const_p tags_tt[]={"Connection","Content-Length","Content-Type",\
								"Cookie","Transfer-Encoding",\
								"Host","Accept","Accept-Charset",\
								"Accept-Encoding","Accept-Language",\
								"Referer","User-Agent","Expect","~"};

Rule to_imp[]={&http_message,&start_line,&status_line,&request_line,&method,&request_target,\
							 &origin_form,&absolute_path,&segment,&query,&absolute_uri,\
							 &scheme,&hier_part,&userinfo,&host,&ipv4address,\
							 &reg_name,&port,&http_version,&http_name,\
							 &content_length_header,&content_type_header,&transfer_encoding_header,&cookie_header,\
							 &host_header,&referer_header,&user_agent_header,&accept_header,\
							 &accept_encoding_header,&accept_charset_header,&accept_language_header,\
							 &connection_header,&expect_header,&content_length,&transfer_encoding,\
							 &transfer_coding,&transfer_extension,&cookie_string,&cookie_pair,\
							 &cookie_name,&cookie_value,&Host,&uri_host,&referer,\
							 &absolute_uri,&partial_uri,&relative_part,&user_agent,&product,\
							 &comment,&media_range,&accept_params,&connection,&connection_option,\
							 &accept_,&accept_charset,&accept_language,&accept_encoding,&codings,&weight};


const_p tags_to_imp[]={"HTTP_message","start_line","status_line","request_line","method","request_target",\
											 "origin_form","absolute_path","segment","query","absolute_URI",\
											 "scheme","hier_part","userinfo","host","IPv4address",\
											 "reg_name","port","HTTP_version","HTTP_name",\
											 "Content_Length_header","Content_Type_header","Transfer_Encoding_header","Cookie_header",\
											 "Host_header","Referer_header","User_Agent_header","Accept_header",\
											 "Accept_Encoding_header","Accept_Charset_header","Accept_Language_header",\
											 "Connection_header","Expect_header","Content_Length","Transfer_Encoding",\
											 "transfer_coding","transfer_extension","cookie_string","cookie_pair",\
											 "cookie_name","cookie_value","Host","Uri_host","Referer",\
											 "absolute_URI","Partial_URI","relative_part","User_Agent","product",\
											 "comment","media_range","Accept_params","Connection","connection_option",\
											 "Accept","Accept_Charset","Accept_Language","Accept_Encoding","codings","weight"};

int is_in(const_p* tab,const_p tag,char t)
{
	int i = 0;
	int max = (t) ? NB_TAG : TO_IMP;
	while (!(rulecase(tab[i],tag)) && i < max){i++;}
	return (i < max) ? i : -1;
}

int is_inn(const_p* tab,const_p tag,char t)
{
	int i = 0;
	int max = (t) ? NB_TAG : TO_IMP;
	while (!(ruleword(tab[i],tag)) && i < max){i++;}
	return (i < max) ? i : -1;
}

const_p _innn_is(const_p test,const_p s)
{
	const_p cursor = test;
	while (cursor && *cursor != ':')
		{
			if ((*cursor|' ') == (*s|' '))
				{
					cursor++;
					s++;
				}
			else if ((*cursor == '_' || *cursor == '_') && (*s == '_' || *s == '_'))
				{
					cursor++;
					s++;
				}
			else cursor = NULL;
		}
	return cursor;
}

int is_innn(const_p* tab,const_p tag,char t)
{
	int i = 0;
	int max = (t) ? NB_TAG : TO_IMP;
	while (!(_innn_is(tag,tab[i])) && i < max){i++;}
	return (i < max) ? i : -1;
}
const_p get_tag(Rule* tab,Rule f,const_p* tabg,char t)
{
	int i = 0;
	int max = (t) ? NB_TAG : TO_IMP;
	while ( f != tab[i] && i < max){i++;}
	return (i < max) ? tabg[i] : NULL;
}

int imp_to_dots(int r)
{
	int to_return;
		switch(r)
		{
			case(31):
					to_return = 0;
					break;
			case(20):
			    to_return = 1;
			    break;
			case(21):
				  to_return = 2;
				  break;
			case(23):
			    to_return = 3;
			    break;
			case(22):
					to_return = 4;
					break;
			case(24):
					to_return = 5;
					break;
			case(27):
					to_return = 6;
					break;
			case(29):
					to_return = 7;
					break;
			case(28):
					to_return = 8;
					break;
			case(30):
			    to_return = 9;
			    break;
			case(25):
				  to_return = 10;
				  break;
			case(26):
			    to_return = 11;
			    break;
			case(32):
					to_return = 12;
					break;
					case(0):
							to_return = 31;
							break;
					case(1):
							to_return = 20;
							break;
					case(2):
							to_return = 21;
							break;
					case(3):
							to_return =23;
							break;
					case(4):
							to_return = 22;
							break;
					case(5):
							to_return = 24;
							break;
					case(6):
							to_return = 27;
							break;
					case(7):
							to_return = 29;
							break;
					case(8):
							to_return = 28;
							break;
					case(9):
							to_return = 30;
							break;
					case(10):
							to_return = 25;
							break;
					case(11):
							to_return = 26;
							break;
					case(12):
							to_return = 32;
							break;
			default:
					to_return = -1;
		}
	return to_return;
}

const_p skip(Rule f,const_p s,const_p end)
{
	if (f == NULL) return NULL;
	while(f(s) == NULL && s != end && f(s) != s ) s++;
	return s;
}

node* toprules(Rule rules[4],Rule except[4],const_p s)
{
	//On verifie avoir quelque chose a traiter
	const_p end = NULL;
	node* to_return = NULL;
	if (s != NULL) end = (rules[0])(s);
	if (end != NULL)
		{
			//allocation
			node* root = malloc(sizeof(node));
			root->next = NULL; //next est utile pour les leaf

			root->tag = get_tag(to_imp,rules[0],tags_to_imp,0);
			root->value = s;		//Premier char qui valide la regle

			root->left = NULL;
			root->mid = NULL;
			root->right = NULL;
			if (root->value == NULL && s == end)	root->len = 0; //Si la regle fait 0 de longueur ex : rulestar(0,0)
			else
				{
					root->len = end-s;

					const_p t = get_tag(with_dots,rules[0],tags_tt,1);
					if (is_in(tags_tt,t,1) != -1)
						{
							s = rulecase(t,s);
							s = rulecase(":",s);
							s = (except[0]) ? (except[0])(s) : s;
						}//Si il y'avait un tag , on l'a skip

					//On se deplace jusqu'a l'endroit où on doit appliquer la sous regle puis on applique.
					root->left = new_node(rules[1],get_tag(to_imp,rules[1],tags_to_imp,0),s);
					s = (root->left != NULL) ? root->left->value + root->left->len : s;
					s = (except[1]) ? (except[1])(s) : s;

					if (rules[2] != NULL)
						{
							root->mid = new_node(rules[2],get_tag(to_imp,rules[2],tags_to_imp,0),s);
							s = (root->mid != NULL) ? root->mid->value + root->mid->len : s;
							s = (except[2]) ? (except[2])(s) : s;
						}

					if (rules[3] != NULL)
						{
							root->right = new_node(rules[3],get_tag(to_imp,rules[3],tags_to_imp,0),s);
							s = (root->mid != NULL) ? root->mid->value + root->mid->len : s;
							s = (except[3]) ? (except[3])(s) : s;
						}
				}
		   to_return = root;
		}
	return to_return;
}

node* new_node(Rule to_apply,const_p tag,const_p s)
{
	//On verifie avoir quelque chose a traiter
	const_p end = NULL;
	node* to_return = NULL;
	if (s != NULL) end = to_apply(s);
	if (end != NULL)
	{
			//allocation
			node* root = malloc(sizeof(node));
			root->next = NULL; //next est utile pour les leaf

			root->tag = tag;
			root->value = s;		//Premier char qui valide la regle

			root->left = NULL;
			root->mid = NULL;
			root->right = NULL;

			if (s == end)	root->len = 0; //Si la regle fait 0 de longueur ex : rulestar(0,0)
			else	root->len = end-s;
		to_return = root;
	}
	return to_return;
}

leaf* new_leaf(Rule to_apply,Rule maybe,const_p tag,const_p tagg,const_p s)
{
	//On verifie avoir quelque chose a traiter
	const_p end = NULL;
	const_p eend = NULL;
	leaf* to_return = NULL;
	if (s != NULL) end = to_apply(s);
	if (end != NULL )
	{
			//allocation
			leaf* root = malloc(sizeof(leaf));
			root->left = NULL;  //
			root->mid = NULL;   // utile pour les node
			root->right = NULL; //


			root->tag = tag;
			root->value = s;		//Premier char qui valide la regle

			if (s == end)	root->len = 0; //Si la regle fait 0 de longueur ex : rulestar(0,0)
			else
				{
					root->len = end-s;
					eend = (maybe) ? maybe(end) : NULL;
					if (eend) root->left = new_node(maybe,tagg,end);
					root->next = new_leaf(to_apply,maybe,tag,tagg,(eend) ? eend : end);
				}
		to_return = root;
	}
	return to_return;
}

leaf* new_leaf_sp(Rule to_apply,Rule maybe,Rule split,const_p tag,const_p tagg,const_p s)
{
	//On verifie avoir quelque chose a traiter
	const_p end = NULL;
	const_p eend = NULL;
	leaf* to_return = NULL;
	if (s != NULL) end = to_apply(s);
	if (end != NULL )
	{
			//allocation
			leaf* root = malloc(sizeof(leaf));
			root->left = NULL;  //
			root->mid = NULL;   // utile pour les node
			root->right = NULL; //


			root->tag = tag;
			root->value = s;		//Premier char qui valide la regle

			if (s == end)	root->len = 0; //Si la regle fait 0 de longueur ex : rulestar(0,0)
			else
				{
					root->len = end-s;
					if (split != NULL) end = split(end);
					eend = (maybe) ? maybe(end) : NULL;
					if (eend) root->left = new_node(maybe,tagg,end);
					root->next = new_leaf_sp(to_apply,maybe,split,tag,tagg,(eend) ? eend : end);
				}
		to_return = root;
	}
	return to_return;
}

void extend_leaf(leaf* root,Rule split,Rule maybe,const_p tagg,const_p s)
{
	if (root)
		{
			int index = is_in(tags_to_imp,root->tag,0);
			if (index != -1)
				{
					Rule f = to_imp[index];
					root->next = new_leaf_sp(f,maybe,split,tags_to_imp[index],NULL,s);
				}
		}
}

void name_it(leaf* root)
{
	if (root)
	{
		int index = is_innn(tags_tt,root->value,1);
		if (index != -1) root->tag = tags_to_imp[imp_to_dots(index)];
		else
			{
				index = is_innn(tags_to_imp,root->value,0);
				if (index != -1) root->tag = tags_to_imp[index];
			}
	}
}

void name_them(leaf* root)
{
	if(root)
	{
		name_it(root);
		name_them(root->next);
	}
}

void clear_node(node* root)
{
	if (root != NULL)
		{
			if (root->left != NULL) free(root->left);
			if (root->mid != NULL) free(root->mid);
			if (root->right != NULL) free(root->right);
			if (root->next != NULL) free(root->next);
			free(root);
		}
}

void show_tree(struct node* root)
{
	if (root != NULL)
	{
		printf("[%d]%s:",root->len,root->tag);
		for (unsigned int i = 0 ; i < root->len ; i++) printf("%c",(root->value)[i]);
		show_tree(root->left);
		show_tree(root->mid);
		show_tree(root->right);
		show_tree(root->next);
						printf("\n");
	}
}

void tab_show_tree(struct node* root, int nbtab)
{
    if (root != NULL)
    {
        int i;
        for (i=0;i<nbtab;i++) printf("\t");
        printf("[%d]%s:",root->len,root->tag);
        for (unsigned int i = 0 ; i < root->len ; i++) printf("%c",(root->value)[i]);
        if (root->left) {printf("\n");for (i=0;i<nbtab;i++) printf("\t");printf("-  -left\n");}
        tab_show_tree(root->left,nbtab+1);
        if (root->mid) {printf("\n");for (i=0;i<nbtab;i++) printf("\t");printf("-  -mid\n");}
        tab_show_tree(root->mid,nbtab+1);
        if (root->right) {printf("\n");for (i=0;i<nbtab;i++) printf("\t");printf("-  -right\n");}
        tab_show_tree(root->right,nbtab+1);
        if (root->next) {printf("\n");for (i=0;i<nbtab;i++) printf("\t");printf("-  -next\n");}
        tab_show_tree(root->next,nbtab);
                        //printf("\n");
    }
}

leaf* noder(leaf* root)
{
	leaf* to_return = NULL;
	if (root)
		{
			int index = is_inn(tags_to_imp,root->tag,0);
			if (index != -1)
				{
					const_p s = root->value;
					leaf* tmp = root->next;
					free(root);
					to_return =(_node[imp_to_dots(index)])(s);
					to_return->next = tmp;
				}
		}
	return to_return;
}
