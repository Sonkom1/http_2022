#include "../includes/syntax.h"

/* Loïs RAJAONSON

v    content-type-header =    "Content-Type" ":" OWS Content-Type OW
v        Content-Type =    media-type
v            media-type =    type "/" subtype * ( OWS ";" OWS parameter )
v                type =    token
v                subtype =   token
v                parameter = token "=" ( token / quoted-string )
v                quoted-string = DQUOTE * ( qdtext / quoted-pair ) DQUOTE
v                    qdtext =    HTAB / SP / "!" / %x23-5B / %x5D-7E / obs-text
v                    quoted-pair =    "\" ( HTAB / SP / VCHAR / obs-text )
*/

// _quoted-pair =  HTAB / SP / VCHAR / obs-text

const_p _quoted_pair(const_p s)
{
    const_p cursor = s;
    cursor = ruleor(NULL,htab,NULL,sp,s);
    if (cursor == NULL)
      {
        cursor = ruleor(NULL,vchar,NULL,obs_text,s);
      }
    return cursor;
}

//quoted-pair =    "\" ( HTAB / SP / VCHAR / obs-text ) == rulecat "\" _quotedpair
const_p quoted_pair(const_p s)
{
    //printf("\n\tquoted-pair :%s",s);
    const_p cursor = rulecat("\\",NULL,NULL,_quoted_pair,s);
    return cursor;
}

//_1qdtext = %x23-5B (== hexa entre 35(#) et 91([))
const_p _1qdtext(const_p s)
{ return (( *s >= 35 && *s <= 91) ? ++s : NULL);}


//_2qdtext = %5D-7E (==  hexa entre 93(]) et 126(~))
const_p _2qdtext(const_p s)
{ return (( *s >= 93 && *s <= 126) ? ++s : NULL);}

//qdtext =    HTAB / SP / "!" / %x23-5B / %x5D-7E / obs-text
const_p qdtext(const_p s)
{
    const_p cursor =  ruleor(NULL,htab,NULL,sp,s);
    if (cursor == NULL)
      {
        cursor = ruleor("!",NULL,NULL,_1qdtext,s);
        if (cursor == NULL)
          {
            cursor = ruleor(NULL,_2qdtext,NULL,obs_text,s);
          }
      }
    return cursor;
}
//_quoted_string = qdtext/quoted-pair
const_p _quoted_string(const_p s)
{
    const_p cursor = ruleor(NULL,qdtext,NULL,quoted_pair,s);
    return cursor;
}

//_star_quoted_string = * (_quoted_string)
const_p _star_quoted_string(const_p s)
{
    const_p cursor = rulestar(NULL,_quoted_string,0,INF,s);
    return cursor;
}

//quoted-string = DQUOTE * ( qdtext / quoted-pair ) DQUOTE
const_p quoted_string(const_p s)
{
    //printf("\n\tquoted-string:%s",s);
    const_p cursor = rulecat(NULL,dquote,NULL,_star_quoted_string,s);
    if (cursor !=NULL)
      {
        cursor = dquote(cursor);
      }
    return cursor;
}

//parameter = token "=" ( token / quoted-string )
const_p parameter(const_p s)
{
    const_p cursor = rulecat(NULL,token,"=",NULL,s);
    if (cursor !=NULL)
      {
        cursor = ruleor(NULL,token,NULL,quoted_string,cursor);
      }
    return cursor;
}

//subtype =   token
const_p subtype(const_p s)
{
    //printf("\n\tsubtype :%s",s);
    const_p cursor = token(s);
    return cursor;
}

//type =   token
const_p type(const_p s)
{
    //printf("\n\ttype :%s",s);
    const_p cursor = token(s);
    return cursor;
}

//_media_type = ( OWS ";" OWS parameter )
const_p _media_type(const_p s)
{
    const_p cursor = rulecat(NULL,ows,";",NULL,s);
    if (cursor !=NULL)
      {
        cursor = rulecat(NULL,ows,NULL,parameter,cursor);
      }
    return cursor;
}

//media-type =    type "/" subtype * ( OWS ";" OWS parameter )
const_p media_type(const_p s)
{
    const_p cursor = rulecat(NULL,type,"/",NULL,s);
    if (cursor !=NULL)
      {
        cursor = subtype(cursor);
        if (cursor != NULL)
          {
            cursor = rulestar(NULL,_media_type,0,INF,cursor);
          }
      }
    return cursor;
}

//content-type =   media-type
const_p content_type(const_p s)
{
    //printf("\n\tcontent-type :%s",s);
    const_p cursor = media_type(s);
    return cursor;
}

//content-type-header =    "Content-Type" ":" OWS Content-Type OWS
const_p content_type_header(const_p s)
{
    //printf("\n\tcontent-type :%s",s);
    const_p cursor = rulecase("content-type", s);
    if (cursor != NULL)
    {
        cursor = rulecat(":", NULL, NULL, ows, cursor);
        if (cursor != NULL)
        {
            cursor = rulecat(NULL, content_type, NULL, ows, cursor);
        }
    }
    return cursor;
}
