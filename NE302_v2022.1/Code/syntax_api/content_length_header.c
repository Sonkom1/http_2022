#include "syntax.h"

/*
connection_length_header = "Content-Length" ":" OWS Content-Length OWS
        Content-Length = 1* DIGIT
*/

//Content-Length = 1* DIGIT
const_p content_length(const_p s)
{
    return rulestar(NULL, digit,1,INF,s);
}


//content_length_string = "Content-Length"
const_p content_length_string(const_p s)
{return rulecase("content-length",s);}


//content_length_header = "Content-Length" ":" OWS Content-Length OWS
const_p content_length_header(const_p s)
{
    const_p cursor = rulecat(NULL, content_length_string, ":", NULL, s);
    if (cursor !=NULL)
    {
        cursor = rulecat(NULL, ows, NULL, content_length,cursor);
        if (cursor !=NULL)
            {
            cursor = ows(cursor);
            }
    }
    return cursor;
}
