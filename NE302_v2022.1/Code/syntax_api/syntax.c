#include "syntax.h"

const_p __(const_p s)
{ return (s != NULL && (*s == '-' || *s == '_')) ? ++s : NULL;}

const_p ruleint(const_p bot,const_p top,const_p s)
{ return (*s >= *bot && *s <= *top) ? ++s : NULL;  }

const_p rulechar(const_p test,const_p s)
{  return ((*test == *s) ? ++s : NULL);  }

const_p ruleword(const_p test,const_p s)
{
    const_p cursor = NULL;
    if(*test == 0 || s == NULL) cursor = s;
    else
      {
        if (*test == *s )
          {
            test++;
            s++;
            cursor = ruleword(test,s);
          }
      }
    return cursor;
}

const_p rulecase(const_p test,const_p s)
{
	const_p cursor = NULL;
    if(*test == 0 || s == NULL || *s == 0) cursor = s;
    else
      {
        if ( *test == *s || (__(test) && __(s)) || ((alpha(s) && ((*test)|' ') == ((*s)|' '))))
          {
            test++;
            s++;
            cursor = rulecase(test,s);
          }
      }
    return cursor;
}

const_p rulecat(const_p test1,\
                const_p (*rule1)(const_p),\
                const_p test2,\
                const_p (*rule2)(const_p),\
                const_p s)
{
  if (test1 != NULL) s = rulecase(test1,s);
  else               s = rule1(s);

  if (s != NULL)
    {
      if (test2 != NULL) s = rulecase(test2,s);
      else               s = rule2(s);
    }
  return s;
}

//Renvoie NULL si le min/max n'est pas respecté , sinon le curseur sur le prochain char;
const_p rulestar(const_p test,\
                 const_p (*rule)(const_p),\
                 unsigned int min,\
                 unsigned int max,\
                 const_p s)
{
  unsigned int count = 0;
  const_p cursor;
  do
  {
      cursor = s;    //On save la pos avant l'appel de la regle
      s = (test == NULL) ? rule(s) : rulecase(test,s);  //Applique la regle si elle renvoie NULL alors c'etait un fail
      if (s != NULL) count++;       //On compte le nombre d'occurence successifs
  }while(s != NULL);
  return (((count < min) || (count > max)) ? NULL : cursor);
}

const_p ruleor(const_p test1,\
               const_p (*rule1)(const_p),\
               const_p test2,\
               const_p (*rule2)(const_p),\
               const_p s)
{
  if ( s != NULL && *s == 0) return NULL;
  const_p cursor = (test1 == NULL) ? rule1(s) : rulecase(test1,s);
  if (cursor == NULL) cursor = (test2 == NULL) ? rule2(s) : rulecase(test2,s);
  return cursor;
}

const_p rulefac(const_p test1,\
                const_p (*rule1)(const_p),\
                const_p test2,\
                const_p (*rule2)(const_p),\
                const_p s)
{
  const_p cursor = (test1 == NULL) ? rule1(s) : rulecase(test1,s);
  s = cursor;
  if (cursor != NULL) cursor = (test2 == NULL) ? rule2(s) : rulecase(test2,s);
  return ((cursor == NULL) ? s : cursor);
}

const_p rulecaf(const_p test1,\
                const_p (*rule1)(const_p),\
                const_p test2,\
                const_p (*rule2)(const_p),\
                const_p s)
{
  const_p cursor = (test1 == NULL) ? rule1(s) : rulecase(test1,s);
  if (cursor == NULL) cursor = (test2 == NULL) ? rule2(s) : rulecase(test2,s);
  else cursor = (test2 == NULL) ? rule2(cursor) : rulecase(test2,cursor);
  return cursor;
}

/* ####################################################### */
//renvoie le curseur sur le prochain char a traiter
const_p cr(const_p s)
{ return ((s != NULL && *s == '\r' ) ? ++s : NULL);}

const_p lf(const_p s)
{ return ((s != NULL && *s == '\n') ? ++s : NULL);}

const_p crlf(const_p s)
{ return rulecat(NULL,cr,NULL,lf,s);}

const_p sp(const_p s)
{ return ((s != NULL && *s == ' ') ? ++s : NULL);}

const_p htab(const_p s)
{ return ((s != NULL && *s == '\t') ? ++s : NULL);}

const_p wsp(const_p s)
{ return ((sp(s) || htab(s)) ? ++s : NULL);}

const_p dquote(const_p s)
{ return ((s != NULL && *s == '"') ? ++s : NULL);}

const_p digit(const_p s)
{ return ((s != NULL && *s >= 48 && *s <= 57) ? ++s : NULL);}

const_p alpha(const_p s)
{ return (((s != NULL && *s >= 65 && *s <= 90) || (*s >= 97 && *s <= 122 )) ? ++s : NULL);}

const_p hex(const_p s)
{ return ((digit(s) || (s != NULL && (*s >= 65 && *s <= 70))) ? ++s : NULL);}

const_p vchar(const_p s)
{ return ((s != NULL && ( *s >= 33 && *s <= 126)) ? ++s : NULL);}

const_p octet(const_p s)
{ return ((s != NULL && ( *s != 0)) ? ++s : NULL);}

const_p obs_text(const_p s)
{ return ((s != NULL && ( *s < 0)) ? ++s : NULL);}

const_p ctl(const_p s)
{ return ((s != NULL && ((*s >= 0 && *s <= 31) || *s == 127)) ? ++s : NULL);}

const_p ows(const_p s)
{ return rulestar(NULL,wsp,0,INF,s);  }

//Renvoie NULL il n'y a pas au min 1 WSP , sinon le curseur sur le prochain char a traiter
const_p rws(const_p s)
{  return rulestar(NULL,wsp,1,INF,s);  }
