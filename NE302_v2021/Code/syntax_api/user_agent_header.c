#include "../includes/syntax.h"

/*
User-Agent-header :
    "User-Agent" ":" OWS User-Agent OWS
	User-Agent = product * ( RWS ( product / comment ) )
		product = token [ "/" product-version ]
			product-version = token
		comment = "(" * ( ctext / quoted-pair / comment ) ")"
			ctext = HTAB / SP / %x21-27 / %x2A-5B / %x5D-7E / obs-text
			quoted-pair = "\" ( HTAB / SP / VCHAR / obs-text )
				obs-text = %x80-FF

User-Agent-header :
	User-Agent
		1*product
			version
		*comment
*/
// ctext = HTAB / SP / %x21-27 / %x2A-5B / %x5D-7E / obs-text
const_p ctext(const_p s)
{
	const_p cursor = wsp(s);
	if (cursor == NULL)	cursor = ruleint("!","'",s);
	if (cursor == NULL)	cursor = ruleint("*","[",s);
	if (cursor == NULL)	cursor = ruleint("]","~",s);
	if (cursor == NULL)	cursor = obs_text(s);
	return cursor;
}

// product-version = token
const_p product_version(const_p s)
{	return token(s);	}

//_comment = ( ctext / quoted-pair / comment )
const_p _comment(const_p s)
{
	const_p cursor = ruleor(NULL,ctext,NULL,quoted_pair,s);
	if (cursor == NULL) cursor = comment(s);
	return cursor;
}

//comment = "(" *(_comment) ")"
const_p comment(const_p s)
{
	const_p cursor = rulechar("(",s);
	if (cursor != NULL) cursor = rulestar(NULL,_comment,0,INF,cursor);
	if (cursor != NULL) cursor = rulechar(")",cursor);
	return cursor;
}

// _product = ("/" product-version)
const_p _product(const_p s)
{	return rulecat("/",NULL,NULL,product_version,s);	}

//product = token [ _product ]
const_p product(const_p s)
{	return rulefac(NULL,token,NULL,_product,s);	}

const_p pro_com(const_p s)
{ return ruleor(NULL,product,NULL,comment,s);	}

//_user_agent = ( RWS ( product / comment ))
const_p _user_agent(const_p s)
{
	const_p cursor = rws(s);
	if ( cursor != NULL) cursor = ruleor(NULL,product,NULL,comment,cursor);
	return cursor;
}

//User-Agent = product * ( _user_agent )
const_p user_agent(const_p s)
{
	const_p cursor = product(s);
	if (cursor != NULL)	cursor = rulestar(NULL,_user_agent,0,INF,cursor);
	return cursor;
}

//"User-Agent" ":" OWS User-Agent OWS
const_p user_agent_header(const_p s)
{
	const_p cursor = rulecase("User-Agent:",s);
	if (cursor != NULL) cursor = ows(cursor);
	if (cursor != NULL) cursor = user_agent(cursor);
	if (cursor != NULL) cursor = ows(cursor);
	return cursor;
}
