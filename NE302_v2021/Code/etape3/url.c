#include "../includes/request_line.h"

// ------------------------------------------------------ //

//malloc
//Supprime les / succesifs
char* slash_removal(char* filepath)
{
  if (filepath == NULL) return NULL;
  int l = 0;
  char* c = filepath;
  char* to_return = malloc((1+strlen(filepath))*sizeof(char));
  while (*c)
  {
    if (!(*(c+1) == '/' && *c == '/')) to_return[l++] = *c;
    c++;
  }

  //free(filepath);
  to_return[l] = 0;
  return to_return;
}

//malloc , eat
//Supprime les ./ et gère les ../
char* dot_removal(char* filepath)
{
   int l = 0;
   char* to_return = malloc((1+strlen(filepath))*sizeof(char));
   char* cursor = next_slash(filepath);
   char* segment;

   char* tmp = to_return;
   char* end = get_last_slash(filepath);

   to_return[l++] = '/';

   while (cursor <= end+1)
   {
     segment = path_segment(cursor);
     if (!segment) break;

     if(ruleword(segment,"../") ||ruleword(segment,".."))
     {
       tmp = previous_slash(get_last_slash(to_return),to_return);
       l = (tmp == to_return) ? 0 : tmp-to_return+1;
       to_return[l++] = '/';
     }
     else if (!ruleword(segment,"./") && !ruleword(segment,"."))
     {  for (int i = 0 ; i < strlen(segment) ; i++) to_return[l++] = segment[i];  }

     free(segment);
     cursor = next_slash(cursor);

   }
   free(filepath);
   to_return[l] = 0;
   return to_return;
}

//b64 -> int
unsigned char transform_b64(char* uri)
{
  int to_return = 0;
  if (uri[1] >= '0' && uri[1] <= '9')
  {      to_return += 16 * atoi((const char*)(uri+1));    }
  else
  {      to_return += 16* (10 + ((uri[1] - 'A')%32) );    }

  if (uri[2] >= '0' && uri[2] <= '9')
  {      to_return += atoi((const char*)(uri+2));    }
  else
  {      to_return += 10 + ((uri[2] - 'A')%32);    }
  return (unsigned char)(to_return%0xFF);
}

//malloc , eat
//Parcourt l'uri et transforme appel transform_b64 quand nécessaire
char* uri_decode(char* uri)
{
  if (uri == NULL) return NULL;
  int l = 0;
  char* c = uri;
  char* to_return = malloc((1+strlen(uri))*sizeof(char));
  for (int i = 0 ; i < strlen(uri) ; i++)
  {
    if (*c == '%')
      {
        to_return[l++] = transform_b64(c);
        c += 2;
      }
    else
      { to_return[l++] = *c;  }
    c++;
  }
  free(uri);
  to_return[l] = 0;
  to_return = realloc(to_return,strlen(to_return)*sizeof(char));
  return to_return;
}

//Malloc , eat
char* transform_uri(char* raw)
{
  char* tmp= uri_decode(raw);
  char* to_return = slash_removal(tmp);
  to_return = dot_removal(to_return);
  free(tmp);
  return to_return;
}
