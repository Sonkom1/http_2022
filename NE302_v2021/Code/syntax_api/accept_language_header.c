#include "../includes/syntax.h"


/* ----------------------------------ACCEPT_LANGUAGE_HEADER---------------------------------- */


// _1qvalue =  "0" [ "." *3 DIGIT ]
const_p _1qvalue (const_p s) {
  const_p cursor = rulechar("0", s);
  if (cursor != NULL) {
    s = cursor;
    cursor = rulechar(".", cursor);
    if (cursor != NULL) cursor = rulestar(NULL,digit,0,3,cursor);
    if (cursor == NULL) cursor = s;
  }
  return cursor;
}

//_2qvalue = ( "1" [ "." *3 "0" ] )
const_p _2qvalue (const_p s) {
  const_p cursor = rulechar("1", s);
  if (cursor != NULL) {
    s = cursor;
    cursor = rulechar(".", cursor);
    //printf("%s\n",cursor);
    if (cursor != NULL) cursor = rulestar("0",NULL,0,3,cursor);
    if (cursor == NULL) cursor = s;
  }
  return cursor;
}

//qvalue : ( "0" [ "." *3 DIGIT ] ) / ( "1" [ "." *3 "0" ] )
const_p qvalue (const_p s)
{ return ruleor(NULL,_1qvalue,NULL,_2qvalue,s); }

//weight : OWS ";" OWS "q=" qvalue
const_p weight (const_p s) {
  const_p cursor = ows(s);
  if (cursor != NULL) cursor = rulecat(";", NULL, NULL, ows, cursor);
  if (cursor != NULL) cursor = rulecat("q=", NULL, NULL, qvalue, cursor);
  return cursor;
}

//alphanum : ALPHA / DIGIT
const_p alphanum (const_p s)
{ return ruleor(NULL,alpha,NULL,digit,s); }

// _1to8alphanum = 1*8 alphanum
const_p _1to8alphanum (const_p s)
{ return rulestar(NULL, alphanum, 1,8,s); }

//_1to8alpha = 1*8 ALPHA
const_p _1to8alpha (const_p s)
{ return rulestar(NULL, alpha, 1,8,s); }

//_quote_and_alphanum = ( "-" 1*8 alphanum )
const_p _quote_and_alphanum (const_p s)
{ return rulecat("-", NULL, NULL, _1to8alphanum, s); }

//_quote_and_alphanum_endless = * ( "-" 1*8 alphanum )
const_p _quote_and_alphanum_endless (const_p s)
{ return rulestar(NULL, _quote_and_alphanum, 0,INF,s); }

//_language_range_alpha =  1*8 ALPHA * ( "-" 1*8 alphanum )
const_p _language_range_alpha (const_p s)
{ return rulecat(NULL, _1to8alpha, NULL, _quote_and_alphanum_endless, s); }

//Language-range : ( 1*8 ALPHA * ( "-" 1*8 alphanum ) ) / "*"
const_p language_range (const_p s)
{ return ruleor(NULL,_language_range_alpha,"*",NULL,s); }

//comma_and_ows = ( "," OWS )
const_p comma_and_ows (const_p s)
{ return rulecat(",", NULL, NULL, ows, s); }

//ows_and_comma = ( OWS "," )
const_p ows_and_comma (const_p s)
{ return rulecat(NULL, ows, ",", NULL, s); }

//endless_comma_and_ows = * ( "," OWS )
const_p endless_comma_and_ows (const_p s)
{ return rulestar(NULL, comma_and_ows, 0,INF,s); }

//_language_range_maybe_weight = ( language-range [ weight ] )
const_p _language_range_maybe_weight (const_p s)
{ return rulefac(NULL, language_range, NULL, weight, s); }

//ows_and_language_range_maybe_weight =  OWS ( language-range [ weight ] )
const_p ows_and_language_range_maybe_weight (const_p s)
{ return rulecat(NULL, ows, NULL, _language_range_maybe_weight, s); }

//single_part2_accept_language = OWS "," [ OWS ( language-range [ weight ] ) ]
const_p single_part2_accept_language (const_p s)
{ return rulefac(NULL, ows_and_comma, NULL, ows_and_language_range_maybe_weight, s); }

//_part1_accept_language = * ( "," OWS ) ( language-range [ weight ] )
const_p _part1_accept_language (const_p s)
{ return rulecat(NULL, endless_comma_and_ows, NULL, _language_range_maybe_weight, s); }

//_part2_accept_language = * ( OWS "," [ OWS ( language-range [ weight ] ) ] )
const_p _part2_accept_language (const_p s)
{ return rulestar(NULL, single_part2_accept_language, 0,INF,s); }

//Accept-Language : * ( "," OWS ) ( language-range [ weight ] ) * ( OWS "," [ OWS ( language-range [ weight ] ) ] )
const_p accept_language (const_p s)
{ return rulecat(NULL, _part1_accept_language, NULL, _part2_accept_language, s); }

//Accept-Language-header : "Accept-Language" ":" OWS Accept-Language OWS
const_p accept_language_header (const_p s) {
  const_p cursor = rulecat("Accept-Language:", NULL, NULL, ows, s);
  if (cursor != NULL) {
    cursor = rulecat(NULL, accept_language, NULL, ows, cursor);
  }
  return cursor;
}
