#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <magic.h>
#include "request.h"
#include "api.h"

#define STATUS_CODES { 200, 400, 404, 411, 501, 505 }
#define NB_STATUS_CODES 6

#define RESPONSE_200 "HTTP/1.1 200 OK"
#define RESPONSE_400 "HTTP/1.1 400 Bad Request"
#define RESPONSE_404 "HTTP/1.1 404 Not Found"
#define RESPONSE_411 "HTTP/1.1 411 Length Required"
#define RESPONSE_501 "HTTP/1.1 501 Not Implemented"
#define RESPONSE_505 "HTTP/1.1 505 HTTP Version Not Supported"
#define RESPONSE_200_LENGTH 15
#define RESPONSE_400_LENGTH 24
#define RESPONSE_404_LENGTH 22
#define RESPONSE_411_LENGTH 28
#define RESPONSE_501_LENGTH 28
#define RESPONSE_505_LENGTH 39
#define RESPONSES { RESPONSE_200, RESPONSE_400, RESPONSE_404, RESPONSE_411, RESPONSE_501, RESPONSE_505 }
#define RESPONSE_LENGTHS { RESPONSE_200_LENGTH, RESPONSE_400_LENGTH, RESPONSE_404_LENGTH, RESPONSE_411_LENGTH, RESPONSE_501_LENGTH, RESPONSE_505_LENGTH }

#define RESPONSE_400_BODY "<!DOCTYPE html><html><head><title>400 - Bad Request</title></head><body><h1>400 - Bad Request</h1><p>Sorry but your request wasn't accepted by the server.</p></body></html>"
#define RESPONSE_404_BODY "<!DOCTYPE html><html><head><title>404 - Not Found</title></head><body><h1>404 - Not Found</h1><p>Sorry but the page you are requesting wasn't found.</p></body></html>"
#define RESPONSE_411_BODY "<!DOCTYPE html><html><head><title>411 - Length Required</title></head><body><h1>411 - Length Required</h1><p>The request contains a payload but no Content-Length header.</p></body></html>"
#define RESPONSE_501_BODY "<!DOCTYPE html><html><head><title>501 - Not Implemented</title></head><body><h1>501 - Not Implemented</h1><p>Sorry but this method is either not implemented by the server or malformed.</p></body></html>"
#define RESPONSE_505_BODY "<!DOCTYPE html><html><head><title>505 - HTTP Version Not Supported</title></head><body><h1>505 - HTTP Version Not Supported</h1><p>Sorry but this HTTP version isn't supported by the server. Its current version is 1.1.</p></body></html>"
#define RESPONSE_400_BODY_LENGTH 172
#define RESPONSE_404_BODY_LENGTH 166
#define RESPONSE_411_BODY_LENGTH 187
#define RESPONSE_501_BODY_LENGTH 202
#define RESPONSE_505_BODY_LENGTH 235

#define RESPONSE_BODIES { RESPONSE_400_BODY, RESPONSE_404_BODY, RESPONSE_411_BODY, RESPONSE_501_BODY, RESPONSE_505_BODY }
#define RESPONSE_BODY_LENGTHS { RESPONSE_400_BODY_LENGTH, RESPONSE_404_BODY_LENGTH, RESPONSE_411_BODY_LENGTH, RESPONSE_501_BODY_LENGTH, RESPONSE_505_BODY_LENGTH }


#define START_LINE 0
#define STATUS_LINE 1
#define METHOD 2
#define TARGET 3
#define CONNECTION 14
#define HTTP_VERSION 4
#define CON_LEN 11
#define HOST 12
#define BODY 15

typedef struct files
{
  const char* fullpath;
  FILE* stream;
  const char* magic;
  magic_t cookie;
}files;

typedef struct field {
  char *name;
  char *content;
} Field;

// ------------------------------------------------------ //
char* next_slash(char* filepath);
char* previous_slash(char* filepath,char* root);
char* path_segment(char* filepath);
char* get_last_slash(char* filepath);

char* get_folder(char* filepath);
char* get_filename(char* filepath);
DIR* open_dir(char* foldername);
void get_filetype(const char* actual_file,files* file);
struct files* open_file(char* filepath,int* php);
void close_file(files* to_close,int full);
char * fileCpyToMain(files * F,int length);
int file_length(char* filename, files* strem_save);
char* error_bodies(int error_code);
// ------------------------------------------------------ //
char* dot_removal(char* filepath);
char* slash_removal(char* filepath);
unsigned char transform_b64(char* uri);
char* uri_decode(char* uri);
char* transform_uri(char* raw);
void show(files* file);
int check_request_line(Field** headers,int* php);
// ------------------------------------------------------ //

Field** get_head();
void init_fields();
void fill_field();
void fill_single_field(int index, char* field_rule);
void print_fields();
char* full_path();
void flush_fields();
void final_flush_fields();
char* read_file(long int* len,char* type);
int traiterRequete(message *req);
