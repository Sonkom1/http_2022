#include "../includes/tree.h"
#include "../includes/parse.h"

#define NBRULES 14
const_p (*rules[])(const_p) = {&start_line,&connection_header,&content_length_header,&content_type_header,\
				 &cookie_header,&transfer_encoding_header,\
				 &host_header,&accept_header,&accept_charset_header,\
				 &accept_encoding_header,&accept_language_header,\
				 &referer_header,&user_agent_header,&expect_header};
const_p c_rules[] = {"Start-line","Connection","Content-Length","Content-Type",\
								"Cookie","Transfer-Encoding",\
								"Host","Accept","Accept-Charset",\
								"Accept-Encoding","Accept-Language",\
								"Referer","User-Agent","Expect"};

const_p read(const_p file)
{
	FILE* In = fopen(file,"r");
	int c,size;
	char* to_return = NULL;
	if (In == NULL)	printf("File not found.\n");
	else
		{
			size = -1;
			do
			{
				c = fgetc(In);
				size++;
			}while(c!= EOF);
			fseek(In,0,SEEK_SET);
			to_return = malloc(size*sizeof(char));
			if (to_return == NULL)	printf("Not able to copy\n");
			else
				{
					for (int i = 0 ; i < size ; i++ ) to_return[i] = fgetc(In);
				}
			fclose(In);
		}
   return (const_p)to_return;
}

void testX(unsigned int x,const_p s)
{
  s = (rules[x])(s);
  if (s == NULL) printf("\t%s fail.\n",c_rules[x]);
  else printf("\t%s OK.\n",c_rules[x]);
}


void test(const_p text)
{
	  const_p buffer = read(text);
  	  if (buffer == NULL) printf("Rien à lire.\n");
  	  else
	  	{
			line* text = dissector(buffer,0);
			while (text != NULL)
			{
				lshow(text->start);
				printf("\n");
				for (int x = 0 ; x < NBRULES ; x++) testX(x,text->start);
				text = text->next;
			}
			clearector(text);
  			free((void*)buffer);
		}
}

int main(int argc , char* argv[])
{ test(argv[1]);return 0;}
