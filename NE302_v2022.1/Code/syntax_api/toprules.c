#include "api.h"


node* connection_node(const_p s)
{
  node* to_return;
  Rule r[4] = {&connection_header,&connection};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  if (to_return != NULL && to_return->len > 0 && to_return->left)
  {
    s = to_return->left->value;
    to_return->left->left = connection_option_node(s);
  }
  return to_return;
}

node* connection_option_node(const_p s)
{
    node* to_return;
    to_return = new_node(&connection_option,"connection-option",s);
    s = connection_option(s);
    while (transfer_coding_split(s)) //ce n'est pas une erreur, le séparateur est le même donc flemme de refaire 2 fois la même fonction.
    {
        s = transfer_coding_split(s);
    }
    s = ows(s);
    if (s) {
        to_return->next = connection_option_node(s);
        }
    else {}
    return to_return;
}

node* content_length_node(const_p s)
{
  node* to_return;
  Rule r[4] = {&content_length_header,&content_length};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  return to_return;
}

node* content_type_node(const_p s)
{
  node* to_return;
  Rule r[4] = {&content_type_header,&content_type};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  if (to_return != NULL && to_return->len > 0 && to_return->left)
  {
    to_return->left->tag = "content-type";
    s = to_return->left->value;
    to_return->left->left = new_node(&media_type,"media-type",s);
  }
  return to_return;
 }

node* cookie_node(const_p s)
{
  node* to_return;
  Rule r[4] = {&cookie_header,&cookie_string};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  if (to_return != NULL && to_return->len > 0 && to_return->left)
  {
    s = to_return->left->value;
    to_return->left->left = cookie_pair_node(s);
  }
  return to_return;
}

node* cookie_pair_node(const_p s)
{
    node* to_return = new_leaf_sp(&cookie_pair,NULL,&cookie_pair_split,"cookie_pair",NULL,s);
    if (to_return)
    {
        to_return->left = new_node(&cookie_name,"cookie_name",s);
        s = cookie_name(s);
        s = ruleword("=",s);
        to_return->mid = new_node(&cookie_value,"cookie_value",s);
        s = cookie_value(s);
        s = cookie_pair_split(s);
        node* parcours = to_return->next;

    while(parcours != NULL)
      {
        parcours->left = new_node(&cookie_name,"cookie_name",s);
        s = cookie_name(s);
        s = ruleword("=",s);
        parcours->mid = new_node(&cookie_value,"cookie_value",s);
        s = cookie_value(s);
        s = cookie_pair_split(s);
        parcours = parcours->next;
      }
    }
    return to_return;
}

node* transfer_encoding_node(const_p s)
{
  node* to_return;
  Rule r[4] = {&transfer_encoding_header,&transfer_encoding};//rules
  Rule e[4] = {&ows,&ows};//exception
  to_return = toprules(r,e,s);
  if (to_return != NULL && to_return->len > 0 && to_return->left)
  {
    s = to_return->left->value;
    //to_return->left->left = new_leaf(&transfer_coding,NULL,"transfer_coding",NULL,s);
    to_return->left->left = transfer_coding_node(s);
  }
  return to_return;
}

node* transfer_coding_node(const_p s)
{
    node* to_return;
    to_return = new_node(&transfer_coding,"transfer-coding",s);
    if (transfer_extension(s) && !rulecase("chunked",s) && !rulecase("compress",s) && !rulecase("gzip",s))
    {
    to_return->left = new_node(&transfer_extension,"transfer_extension",s);
    }
    s = transfer_coding(s);
    while (transfer_coding_split(s))
    {
        s = transfer_coding_split(s);
    }
    s = ows(s);
    if (s) {
        to_return->next = transfer_coding_node(s);
        }
    else ;
    return to_return;
}

node* host_node(const_p s)
{
    node* to_return;
    Rule r[4] = {&host_header,&Host};//rules
    Rule e[4] = {&ows,&ows};//exception
    to_return = toprules(r,e,s);
    if (to_return != NULL && to_return->len > 0 && to_return->left)
      {
        s = to_return->left->value;
        to_return->left->left = new_node(reg_name,"reg_name",s);
        if (to_return->left->left == NULL) to_return->left->left = new_node(ipv4address,"IPv4address",s);
        if (to_return->left->left == NULL) to_return->left->left = new_node(ip_literal,NULL,s);
      }
    return to_return;
  }

node* expect_node(const_p s)
{
  Rule r[4] = {&expect_header,&expect};//rules
  Rule e[4] = {&ows,&ows};//exception
  return toprules(r,e,s);
}

node* request_line_node(const_p s)
{
  Rule r[4] = {&request_line,&method,&request_target,&http_version};
  Rule e[4] = {NULL,&sp,&sp,&crlf};
  return toprules(r,e,s);
}

node* status_line_node(const_p s)
{
  Rule r[4] = {&status_line,&http_version,&status_code,&reason_phrase};
  Rule e[4] = {NULL,&sp,&sp,&crlf};
  return toprules(r,e,s);
}

node* http_message_node(const_p s)
{
  node* to_return;
  node* cursor;
  Rule r[4] = {&http_message,&start_line};
  Rule e[4] = {NULL,NULL};
  to_return = toprules(r,e,s);
  if (to_return)
    {
      //Pour start-line
      to_return->left->left = (request_line(s)) ? request_line_node(s) : status_line_node(s);
      s = to_return->left->value + to_return->left->len;
      //Pour les header-fields
      to_return->mid = new_leaf(&_header_fields,NULL,NULL,NULL,s);
      if (to_return->mid)
      {
        if(implemented_only(s))
            {
            name_it(to_return->mid);
            to_return->mid = noder(to_return->mid);
            cursor = to_return->mid;
            s = _header_fields(s);
            } else{
                to_return->mid->tag = nomDuHeader(s);
                to_return->mid->left = new_node(&field_name,"field_name",s);



                to_return->mid->value = s;
                to_return->mid->len = _other(s) - s;

                s = ruleword(to_return->mid->tag,s);
                s = ruleword(":",s);
                s  = ows(s);
                to_return->mid->mid = new_node(&field_value,"field_value",s);
                s = field_value(s);
                s = ows(s);
                s = crlf(s);
                cursor = to_return->mid;
            }
            while(cursor && cursor->next)
                {
                if(implemented_only(s))
                    {
                    if (cursor->next->tag == NULL){name_it(cursor->next);}
                    cursor->next = noder(cursor->next);
                    cursor = cursor->next;
                    s = _header_fields(s);
                    }

                else
                    {




                    cursor->next->tag = nomDuHeader(s); //je nomme le header non implémenté

                    cursor->next->left = new_node(&field_name,"field_name",s);


                    s = ruleword(cursor->next->tag,s);
                    s = ruleword(":",s);
                    s  = ows(s); //j'avance s jusqu'au field-value de ce header non implémenté

                    cursor->next->mid = new_node(&field_value,"field_value",s);
                    cursor->next->value = s; //la valeur du noeud du header non implémenté est mise à s
                    cursor->next->len = field_value(s) - s; //sa longueur est mise à field_value(s) - s

                    s = field_value(s); //on avance s après la field-value
                    s = ows(s);
                    s = crlf(s);
                    cursor = cursor->next;//le curseur passe au suivant






                    }
                //s = _header_fields(s);
                }
            s = cursor->value + cursor->len;
            crlf(s);
            }
        }
      //Pour le body
      s = crlf(s);
      if (s)
        {
          to_return->right = new_node(&message_body,"body",s);
        }
  return to_return;
}
