#include "../includes/request_line.h"
#define FLEN 16
#define PATH_LENGTH 200
#define CONTENT_LEN 1024
Field* fields_header[FLEN];
char* fields_names[FLEN] = {"Start-line","status_line","Method","Target","HTTP_version","Transfer_Encoding_header", "Cookie_header", "Referer_header", "User_Agent_header",\
                        "Accept_header", "Accept_Encoding_header",\
                        "Content_Length-header", "Host_header","Port","Connection_header","Body"};
char* fields_rules[FLEN] = {"start_line","status_line","method","request_target","HTTP_version","Transfer_Encoding", "cookie_string", "Referer", "User_Agent",\
                        "Accept", "Accept_Encoding",\
                        "Content_Length", "Host","port","Connection","body"};
/* A refaire
Tableau statiques des champs, index :
0 : Start-line
1 : Status-line
2 : Method
3 : Target
4 : HTTP_version
5 : Transfer-Encoding-Header
6 : Cookie-Header
7 : Referer-Header
8 : User-Agent-Header
9 : Accept-Header
10 : Accept-Encoding-Header
11 : Content-Lenth-Header
12 : Host-Header
13 : Port
14 : Connection-Header
15 : Body
*/


//Fonction verifiant s'il n'y a bien qu'un champ unique dans la requête
//Exemple : Il n'y peut pas avoir deux champs Accept-header
void print_fields() {
  for (int i = 0; i < FLEN; i++){
    printf("%s - Content : %s\n",fields_header[i]->name,fields_header[i]->content);
  }
}

int check_single_field(_Token *token_list) {
  int check = 1;
  if (token_list == NULL) check = 0;
  else if (token_list->next != NULL) check = 0;
  return check;
}

//Fonction retournant le pointeur vers une structure correspondant au champ selectionné
//L'unicité de ce champ sera vérifié au préalable
void fill_single_field(int index, char* field_rule) {
  int len;
  char* fillname;
  _Token *token_list = searchTree(getRootTree(),field_rule);
  struct node *nod;
  if (check_single_field(token_list)) {
    nod = token_list->node;
    fillname = getElementValue(nod,&len);
    if (len > CONTENT_LEN) fields_header[index]->content = (char *)realloc(fields_header[index]->content, sizeof(char) * len);
    strncpy(fields_header[index]->content, fillname, len);
  }
  purgeElement(&token_list);
}

void flush_fields() {
  for (int i = 0; i < FLEN; i++) if (fields_header[i]) {
    fields_header[i]->content = (char *)realloc(fields_header[i]->content, sizeof(char) * CONTENT_LEN);
    for (int a = 0; a < CONTENT_LEN; a++) (fields_header[i]->content)[a] = '\0';
  }
}

void init_fields() {
  for (int i = 0; i < FLEN; i++) {
    fields_header[i] = (Field *)malloc(sizeof(Field));
    fields_header[i]->name = fields_names[i];
    fields_header[i]->content = (char *)malloc(sizeof(char) * CONTENT_LEN);
    (fields_header[i]->content)[0] = 0;
  }
}

void final_flush_fields() {
  for (int i = 0; i < FLEN; i++) if (fields_header[i]) {
    if (fields_header[i]->content) free(fields_header[i]->content);
    free(fields_header[i]);
    fields_header[i] = NULL;
  }
}

/*
void flush_fields() {
  for (int i = 0; i < FLEN; i++) if (fields_header[i]) {
    if (fields_header[i]->name) {
      free(fields_header[i]->name);
      fields_header[i]->name = NULL;
    }
    if (fields_header[i]->content) {
      free(fields_header[i]->content);
      fields_header[i]->content = NULL;
    }
    free(fields_header[i]);
    fields_header[i] = NULL;
  }
}
*/

void fill_field() {
  flush_fields();
  for (int i = 0; i < FLEN; i++) fill_single_field(i, fields_rules[i]);
}

Field** get_head()
{return fields_header;}

int check_body_len()
{
    if (fields_header[CON_LEN]->content && fields_header[BODY]->content && strlen(fields_header[BODY]->content) == atoi(fields_header[CON_LEN]->content)) return 100;
    return 411;
}

//Full path form : /var/www/"hostname"/Target
//hostname form : "smthg_site.zone"
char *full_path() {
  char* true_path = (char*)malloc(sizeof(char) * PATH_LENGTH);
  strcpy(true_path, "/var/www");
  char *hostpath, *hostname, *target;
  int len_hostname, dot_index, dot_index_2, len_target, success;
  char check_dot = 2;

  if (fields_header[HOST]->content[0] != 0) {
    hostname = (char *)malloc(sizeof(fields_header[HOST]->content));
    strcpy(hostname, fields_header[HOST]->content);

    if (ruleor(NULL,ip_literal,NULL,ipv4address,hostname)) {
      hostpath = (char *)malloc(sizeof(char) * 9);
      strcpy(hostpath,"/default");
    } else {
      len_hostname = strlen(hostname) - strlen(uri_host(hostname));
      hostpath = (char *)malloc(sizeof(char) * len_hostname);
      hostpath[0] = 0;

      dot_index = len_hostname-1;
      dot_index_2 = dot_index;
      while(dot_index >= 0) {
        if ((hostname[dot_index] == '.') || !dot_index){
          if ((dot_index == 3) && (strncmp("www",hostname,dot_index) == 0)) {
            strcat(hostpath,"_");
            strncat(hostpath,(hostname + dot_index + 1), (dot_index_2 - dot_index));
            break;
          }

          if(check_dot) {
            strcat(hostpath,"/");
            check_dot--;
          } else strcat(hostpath,"_");

          if(dot_index) strncat(hostpath,(hostname + dot_index + 1), (dot_index_2 - dot_index));
          else strncat(hostpath, hostname, dot_index_2+1);
          dot_index_2 = dot_index-1;
        }
        dot_index--;
      }
    }
    strcat(true_path, hostpath);
    free(hostpath);
    hostpath = NULL;
  } else {
    strcat(true_path, "/default\0");
  }

  if (fields_header[TARGET]->content[0] != '\0') {
    target = (char *)malloc(sizeof(fields_header[TARGET]->content)+1);
    strcpy(target,fields_header[TARGET]->content);
    strcat(target," ");
    len_target = strlen(target) - strlen(absolute_path(target));

    success = strncmp("/", target, len_target);
    if ((len_target <= 1) && !success) strcat(true_path, "/index.html\0");
    else strncat(true_path, target, len_target);
    free(target);
    target = NULL;
  }
  return true_path;
}
