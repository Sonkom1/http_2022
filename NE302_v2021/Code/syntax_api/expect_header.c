#include "../includes/syntax.h"
/*
Expect-header :
    "Expect" ":" OWS Expect OWS
	Expect = "100-continue"
*/

// Expect = "100-continue"
const_p expect(const_p s)
{ return rulecase("100-continue",s); }


//Expect-header : "Expect" ":" OWS Expect OWS
const_p expect_header(const_p s)
{
	const_p cursor = rulecat("Expect:",NULL,NULL,ows,s);
	if (cursor != NULL) cursor = rulecat(NULL,expect,NULL,ows,cursor);
	return cursor;
}
