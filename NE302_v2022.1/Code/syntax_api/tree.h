#include <stdlib.h>
#include <stdio.h>
#include "syntax.h"
#ifndef ALL
#define ALL 0

#endif

typedef struct node node,leaf;
struct node
{
	//Data
	const char* tag;
	const char* value;
	int len;

	//Mobilite descendante
	node* left;
	node* mid;
	node* right;

	//Mobilite horizontale
	leaf* next;
}*r00t;

void _(void);
const_p skip(Rule f,const_p s,const_p end);
leaf* new_leaf(Rule to_apply,Rule maybe,const_p tag,const_p tagg,const_p s);
leaf* new_leaf_sp(Rule to_apply,Rule maybe,Rule split,const_p tag,const_p tagg,const_p s);
void extend_leaf(leaf* root,Rule split,Rule maybe,const_p tagg,const_p s);
void name_it(leaf* root);
void name_them(leaf* root);
leaf* noder(leaf* root);

node* new_node(Rule to_apply,const_p tag,const_p s);
void show_tree(struct node* root);
void tab_show_tree(struct node* root, int nbtab);
void clear_node(node* root);

int is_in(const_p* tab,const_p tag,char t);//rulecase
int is_inn(const_p* tab,const_p tag,char t);//ruleword
const_p get_tag(Rule* tab,Rule f,const_p* tabg,char t);
int imp_to_dots(int r);
node* toprules(Rule rules[4],Rule except[4],const_p s);
node* content_length_node(const_p s);
node* content_type_node(const_p s);


node* transfer_encoding_node(const_p s);
node* transfer_coding_node(const_p s);
node* cookie_node(const_p s);
node* cookie_pair_node(const_p s);
node* connection_node(const_p s);
node* connection_option_node(const_p s);
node* host_node(const_p s);
node* expect_node(const_p s);
node* request_line_node(const_p s);
node* status_line_node(const_p s);
node* http_message_node(const_p s);
