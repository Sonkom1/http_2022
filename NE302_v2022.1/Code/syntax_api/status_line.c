#include "syntax.h"

/*
status-line = HTTP-version SP status-code SP reason-phrase CRLF
  HTTP-version = HTTP-name "/" DIGIT "." DIGIT
  status-code = 3 DIGIT
  reason-phrase = * ( HTAB / SP / VCHAR / obs-text )
    obs-text = %x80-FF
*/


//_reason-phrase =  HTAB / SP / VCHAR / obs-text
const_p _reason_phrase(const_p s)
{
    const_p cursor = ruleor(NULL,htab,NULL,sp,s);
    if ( cursor == NULL)
      {
        cursor = ruleor(NULL,vchar,NULL,obs_text,s);
      }
    return cursor;
}

//reason_phrase = *(reason-phrase)
const_p reason_phrase(const_p s)
{
    //printf("\n\treason_phrase :%s",s);
    const_p cursor = rulestar(NULL,_reason_phrase,0,INF,s);
    return cursor;
}
//status-code = 3 DIGIT
const_p status_code(const_p s)
{
    //printf("\n\tstatus_code :%s",s);
    const_p cursor = rulestar(NULL,digit,3,3,s);
    return cursor;
}

//status-line = HTTP-version SP status-code SP reason-phrase CRLF
const_p status_line(const_p s)
{
    const_p cursor = rulecat(NULL,http_version,NULL,sp,s);
    if ( cursor != NULL)
    {
      cursor = rulecat(NULL,status_code,NULL,sp,cursor);
      if ( cursor != NULL)
        {
          cursor = rulecat(NULL,reason_phrase,NULL,crlf,cursor);
        }
    }
    return cursor;
}
