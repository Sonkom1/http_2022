#include "../includes/request_line.h"
#include "../includes/zlib.h"

int check_method(char* method)
{
  const char* m = method;
  if (ruleword(m,"GET")) return 100;
  if (ruleword(m,"POST")) return 100;
  if (ruleword(m,"HEAD")) return 100;
  return 501;
}

int check_version(char* http_version,char* host)
{
  if (*http_version) return 400;
  const char* v = http_version+5;
  if (ruleword(v,"1.1")) return (host[0]) ? 100 : 400;
  if (ruleword(v,"1.0")) return 100;
  return 505;
}

/////////////////////////////////////////
int check_file(char* filepath,char* method)
{
  int to_return = 100;
  char* path = filepath;
  files* ressources = open_file(path);
  if (ressources == NULL) return 404;
  else
  {
    if (ruleword("GET",method) || ruleword("HEAD",method))
    { to_return = (ressources->stream) ? 100 : 404; }//Si un fichier n'a pas ete ouvert en lecture c'est qu'il existe pas /pas les droits
    close_file(ressources,0);
  }
  return to_return;
}
/////////////////////////////////////////
int check_request(Field** headers)
{
  if (!(headers[METHOD]->content[0] && headers[TARGET]->content[0] && headers[HTTP_VERSION]->content[0])) return 400;
  int met = check_method(headers[METHOD]->content);
  if (met != 100) return met;
  int ver = check_version(headers[HTTP_VERSION]->content,headers[HOST]->content);
  if (ver != 100) return ver;
  headers[TARGET]->content = transform_uri(headers[TARGET]->content);
  headers[TARGET]->content = full_path();
  int fil = check_file(headers[TARGET]->content,headers[METHOD]->content);
  if (fil != 100) return fil;
  return 200;
}

int check_status(Field* line)
{
  char* cursor = line->content;
  while (*cursor && *cursor++ != ' ');//On skip jusqu'au premier ' '
  return atoi(cursor)*100+atoi(cursor+1)*10+atoi(cursor+2);
}

int check_request_line(Field** headers)
{
  if (headers[START_LINE]->content[0] == 0) return 400;
  if (headers[STATUS_LINE]->content[0] != 0) return check_status(headers[STATUS_LINE]);
  else return check_request(headers);
}

char* error_bodies(int error_code) {
  char* return_error;
  switch (error_code)
  {
      case 400: return_error = RESPONSE_400_BODY; break;
      case 404: return_error = RESPONSE_404_BODY; break;
      case 411: return_error = RESPONSE_411_BODY; break;
      case 501: return_error = RESPONSE_501_BODY; break;
      case 505: return_error = RESPONSE_505_BODY; break;
  }
  return return_error;
}



void testz()
{
  FILE* Out = fopen("url.def","wb");
  FILE* In = fopen("url.c","rb");
  FILE* Outt = fopen("url.def.c","wb");
  def(In,Out,Z_DEFAULT_COMPRESSION);
  fclose(In);
  fclose(Out);

  Out = fopen("url.def","rb");
  inf(Out,Outt);
  fclose(Out);
  fclose(Outt);
}

/*int main(int argc,char* argv[])
{
  const char* sa = "GET /mnt/c/Users/Lamantin/Desktop/ne_px/../ne_px/Code/etape3/index.html HTTP/1.0\r\nHost: mail.esisar.grenoble-inp.fr\r\n\r\nLama.";
  const char* so = "HTTP/1.1 200 OK\r\n\r\n";
  char* t = malloc(strlen("GET /mnt/c/Users/Lamantin/Desktop/ne_px/../ne_px/Code/etape3/index.html HTTP/1.0\r\nHost: mail.esisar.grenoble-inp.fr\r\n\r\nLama.")*sizeof(char));
  strcpy(t,"GET /mnt/c/Users/Lamantin/Desktop/ne_px/../ne_px/Code/etape3/index.html HTTP/1.0\r\nHost: mail.esisar.grenoble-inp.fr\r\n\r\nLama.");
  int success = parseur((char *)t,0);
  if (success)
  {    fill_field(); }
  else return -1;
  Field** headers = get_head();
  success = check_request_line(headers);
  print_fields();
  purgeTree(getRootTree());
  return 0;
}*/
