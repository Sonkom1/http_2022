#include "../includes/ffcgi.h"

int main(int argc, char *argv[])
{
	message *requete;
    init_fields();
	while ( 1 ) {
		// on attend la reception d'une requete HTTP requete pointera vers une ressource allouée par librequest.
		if ((requete=getRequest(8080)) == NULL ) return -1;

		// Affichage de debug
		printf("#########################################\nDemande recue depuis le client %d\n",requete->clientId);
		printf("Client [%d] [%s:%d]\n",requete->clientId,inet_ntoa(requete->clientAddress->sin_addr),htons(requete->clientAddress->sin_port));
		printf("Contenu de la demande:\n%.*s\n\n",requete->len,requete->buf);

		if (traiterRequete(requete)) {printf("oops!\n");}
		freeRequest(requete);
  }
  final_flush_fields();
}
