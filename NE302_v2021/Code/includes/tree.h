#include <stdlib.h>
#include <stdio.h>
#include "syntax.h"
#ifndef ALL
#define ALL 0

#endif

typedef struct node node,leaf;
struct node
{
	//Data
	const char* tag;
	const char* value;
	int len;

	//Mobilite descendante
	node* left;
	node* mid;
	node* right;

	//Mobilite horizontale
	leaf* next;
}*r00t;

void _(void);
const_p skip(Rule f,const_p s,const_p end);
leaf* new_leaf(Rule to_apply,Rule maybe,const_p tag,const_p tagg,const_p s);
leaf* new_leaf_sp(Rule to_apply,Rule maybe,Rule split,const_p tag,const_p tagg,const_p s);
void extend_leaf(leaf* root,Rule split,Rule maybe,const_p tagg,const_p s);
void name_it(leaf* root);
void name_them(leaf* root);
leaf* noder(leaf* root);

node* new_node(Rule to_apply,const_p tag,const_p s);
void show_tree(struct node* root);
void tab_show_tree(struct node* root, int nbtab);
void clear_node(node* root);

int is_in(const_p* tab,const_p tag,char t);//rulecase
int is_inn(const_p* tab,const_p tag,char t);//ruleword
const_p get_tag(Rule* tab,Rule f,const_p* tabg,char t);
int imp_to_dots(int r);
node* toprules(Rule rules[4],Rule except[4],const_p s);
//Segfault
node* content_length_node(const_p s);
node* content_type_node(const_p s);


node* accept_charset_node(const_p s);
node* accept_encoding_node(const_p s);
node* accept_language_node(const_p s);
//Segfault
//Incomplet
node* referer_node(const_p s);
//Incomplet
//Fini
node* transfer_encoding_node(const_p s);
node* transfer_coding_node(const_p s);
node* cookie_node(const_p s);
node* cookie_pair_node(const_p s);
node* connection_node(const_p s);
node* connection_option_node(const_p s);
node* host_node(const_p s);
node* accept_node(const_p s);
node* user_agent_node(const_p s);
node* expect_node(const_p s);
node* request_line_node(const_p s);
node* status_line_node(const_p s);
node* http_message_node(const_p s);
//Fini
void check_nodes(void);
/*
request-line :
    method SP request-target SP HTTP-version CRLF
	request-target = origin-form
		origin-form = absolute-path [ "?" query ]
			absolute-path = 1* ( "/" segment )
				segment = * pchar
					pchar = unreserved / pct-encoded / sub-delims / ":" / "@"
						unreserved = ALPHA / DIGIT / "-" / "." / "_" / "~"
						pct-encoded = "%" HEXDIG HEXDIG
						sub-delims = "!" / "$" / "&" / "'" / "(" / ")" / "*" / "+" / "," / ";" / "="
			query = * ( pchar / "/" / "?" )
	HTTP-version = HTTP-name "/" DIGIT "." DIGIT
		HTTP-name = %x48.54.54.50

request-line
	request-target
		origin-form
			absolute-path
			* query
	HTTP-version
		HTTP-name


status-line :
    HTTP-version SP status-code SP reason-phrase CRLF
	HTTP-version = HTTP-name "/" DIGIT "." DIGIT
		HTTP-name = %x48.54.54.50
	status-code = 3 DIGIT
	reason-phrase = * ( HTAB / SP / VCHAR / obs-text )
		obs-text = %x80-FF

status-line :
	HTTP-version
		HTTP-name
	status-code
	reason-phrase
		*""""mots"""

connection-header :
   "Connection" ":" OWS Connection OWS
	Connection = * ( "," OWS ) connection-option * ( OWS "," [ OWS connection-option ] )
		connection-option = token
			token = 1* tchar
				tchar = "!" / "#" / "$" / "%" / "&" / "'" / "*" / "+" / "-" / "." / "^" / "_" / "`" / "|" / "~" / DIGIT / ALPHA

connection-header :
	1*connection-option

content-Length-header :
    "Content-Length" ":" OWS Content-Length OWS
	Content-Length = 1* DIGIT

content-Length-header :
	Content-Length

content-type-header :
    "Content-Type" ":" OWS Content-Type OWS
	Content-Type = media-type
		media-type = type "/" subtype * ( OWS ";" OWS parameter )
			type = token
			subtype = token
			parameter = token "=" ( token / quoted-string )
				quoted-string = DQUOTE * ( qdtext / quoted-pair ) DQUOTE
					qdtext = HTAB / SP / "!" / %x23-5B / %x5D-7E / obs-text
					quoted-pair = "\" ( HTAB / SP / VCHAR / obs-text )
						obs-text = %x80-FF
content-type-header :
	media-type
		type
		subtype
		*(parameter)


cookie-header :
    "Cookie:" OWS cookie-string OWS
	cookie-string = cookie-pair * ( ";" SP cookie-pair )
		cookie-pair = cookie-name "=" cookie-value
			cookie-name = token
			cookie-value = ( DQUOTE * cookie-octet DQUOTE ) / * cookie-octet
				cookie-octet = %x21 / %x23-2B / %x2D-3A / %x3C-5B / %x5D-7E

cookie-header :
	cookie-string
		1*cookie-pair
			cookie-name
			cookie-value
				*cookie-octet


Transfer-Encoding-header :
    "Transfer-Encoding" ":" OWS Transfer-Encoding OWS
	Transfer-Encoding = * ( "," OWS ) transfer-coding * ( OWS "," [ OWS transfer-coding ] )
		transfer-coding = "chunked" / "compress" / "deflate" / "gzip" / transfer-extension
			transfer-extension = token * ( OWS ";" OWS transfer-parameter )
				transfer-parameter = token BWS "=" BWS ( token / quoted-string )
					BWS = OWS
					quoted-string = DQUOTE * ( qdtext / quoted-pair ) DQUOTE
						qdtext = HTAB / SP / "!" / %x23-5B / %x5D-7E / obs-text
						quoted-pair = "\" ( HTAB / SP / VCHAR / obs-text )
							obs-text = %x80-FF
Transfer-Encoding-header :
	1*transfer-coding


Expect-header :
    "Expect" ":" OWS Expect OWS
	Expect = "100-continue"

Host-header :
    "Host" ":" OWS Host OWS
	host = IP-literal / IPv4address / reg-name
		IP-literal = "[" ( IPv6address / IPvFuture ) "]"
			IPv6address = 6 ( h16 ":" ) ls32 / "::" 5 ( h16 ":" ) ls32 / [ h16 ] "::" 4 ( h16 ":" ) ls32 / [ h16 *1 ( ":" h16 ) ] "::" 3 ( h16 ":" ) ls32 / [ h16 *2 ( ":" h16 ) ] "::" 2 ( h16 ":" ) ls32 / [ h16 *3 ( ":" h16 ) ] "::" h16 ":" ls32 / [ h16 *4 ( ":" h16 ) ] "::" ls32 / [ h16 *5 ( ":" h16 ) ] "::" h16 / [ h16 *6 ( ":" h16 ) ] "::"
			IPvFuture = "v" 1* HEXDIG "." 1* ( unreserved / sub-delims / ":" )
		IPv4address = dec-octet "." dec-octet "." dec-octet "." dec-octet
			dec-octet = "25" %x30-35 / "2" %x30-34 DIGIT / "1" 2 DIGIT / %x31-39 DIGIT / DIGIT
		reg-name = * ( unreserved / pct-encoded / sub-delims )
			unreserved = ALPHA / DIGIT / "-" / "." / "_" / "~"
			pct-encoded = "%" HEXDIG HEXDIG
			sub-delims = "!" / "$" / "&" / "'" / "(" / ")" / "*" / "+" / "," / ";" / "="
Host-header :
	host
		IP-literal
		IPv4address
		reg-name

Accept-header :
    "Accept" ":" OWS Accept OWS
	Accept = [ ( "," / ( media-range [ accept-params ] ) ) * ( OWS "," [ OWS ( media-range [ accept-params ] ) ] ) ]
*///	media-range = ( "*/*" / ( type "/" subtype ) / ( type "/*" ) ) * ( OWS ";" OWS parameter )
/*			type = token
			subtype = token
			parameter = token "=" ( token / quoted-string )
				quoted-string = DQUOTE * ( qdtext / quoted-pair ) DQUOTE
					qdtext = HTAB / SP / "!" / %x23-5B / %x5D-7E / obs-text
					quoted-pair = "\" ( HTAB / SP / VCHAR / obs-text )
						obs-text = %x80-FF
		accept-params = weight * accept-ext
			weight = OWS ";" OWS "q=" qvalue
				qvalue = ( "0" [ "." *3 DIGIT ] ) / ( "1" [ "." *3 "0" ] )
			accept-ext = OWS ";" OWS token [ "=" ( token / quoted-string ) ]
				quoted-string = DQUOTE * ( qdtext / quoted-pair ) DQUOTE
					qdtext = HTAB / SP / "!" / %x23-5B / %x5D-7E / obs-text
					quoted-pair = "\" ( HTAB / SP / VCHAR / obs-text )
Accept-header :
	Accept
		*media-range:accept-params

Accept-Charset-header :
    "Accept-Charset" ":" OWS Accept-Charset OWS
	Accept-Charset = * ( "," OWS ) ( ( charset / "*" ) [ weight ] ) * ( OWS "," [ OWS ( ( charset / "*" ) [ weight ] ) ] )
		charset = token
		weight = OWS ";" OWS "q=" qvalue
				qvalue = ( "0" [ "." *3 DIGIT ] ) / ( "1" [ "." *3 "0" ] )


Accept-Charset-header :
	Accept-Charset
		*charset:weight


Accept-Encoding-header :
    "Accept-Encoding" ":" OWS Accept-Encoding OWS
	Accept-Encoding = [ ( "," / ( codings [ weight ] ) ) * ( OWS "," [ OWS ( codings [ weight ] ) ] ) ]
		codings = content-coding / "identity" / "*"
			content-coding = token
		weight = OWS ";" OWS "q=" qvalue
			qvalue = ( "0" [ "." *3 DIGIT ] ) / ( "1" [ "." *3 "0" ] )

Accept-Encoding-header :
	Accept-Encoding
		*codings:weight

Accept-Language-header :
    "Accept-Language" ":" OWS Accept-Language OWS
	Accept-Language = * ( "," OWS ) ( language-range [ weight ] ) * ( OWS "," [ OWS ( language-range [ weight ] ) ] )
		language-range = ( 1*8 ALPHA * ( "-" 1*8 alphanum ) ) / "*"
			alphanum = ALPHA / DIGIT
		weight = OWS ";" OWS "q=" qvalue
			qvalue = ( "0" [ "." *3 DIGIT ] ) / ( "1" [ "." *3 "0" ] )

Accept-Language-header:
	Accept-Language
		*language-range:weight

Referer-header :
    "Referer" ":" OWS Referer OWS
	Referer = absolute-URI / partial-URI
		absolute-URI = scheme ":" hier-part [ "?" query ]
		partial-URI = relative-part [ "?" query ]
			relative-part = "//" authority path-abempty / path-absolute / path-noscheme / path-empty
			scheme = ALPHA * ( ALPHA / DIGIT / "+" / "-" / "." )
			hier-part = "//" authority path-abempty / path-absolute / path-rootless / path-empty
				authority = [ userinfo "@" ] host [ ":" port ]
					userinfo = * ( unreserved / pct-encoded / sub-delims / ":" )
					host = ...
					port = * DIGIT
				path-abempty = * ( "/" segment )
				path-absolute = "/" [ segment-nz * ( "/" segment ) ]
				path-rootless = segment-nz * ( "/" segment )
	ew_node			path-noscheme = segment-nz-nc * ( "/" segment )
				path-empty = ""
					segment-nz = 1* pchar
					segment-nz-nc = 1* ( unreserved / pct-encoded / sub-delims / "@" )
						unreserved = ALPHA / DIGIT / "-" / "." / "_" / "~"
						pct-encoded = "%" HEXDIG HEXDIG
						sub-delims = "!" / "$" / "&" / "'" / "(" / ")" / "*" / "+" / "," / ";" / "="

Referer-header :
	Referer
		absolute-URI
			scheme
			hier-part
				authority
					userinfo
					host
					port
			query
		partial-URI
			relative-part
				authority
					userinfo
					host
					port
			query

User-Agent-header :
    "User-Agent" ":" OWS User-Agent OWS
	User-Agent = product * ( RWS ( product / comment ) )
		product = token [ "/" product-version ]
			product-version = token
		comment = "(" * ( ctext / quoted-pair / comment ) ")"
			ctext = HTAB / SP / %x21-27 / %x2A-5B / %x5D-7E / obs-text
			quoted-pair = "\" ( HTAB / SP / VCHAR / obs-text )
				obs-text = %x80-FF

User-Agent-header :
	User-Agent
		1*product
			version
		*comment

*/
